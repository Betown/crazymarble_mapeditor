﻿//  --------------------------------
//  Copyright (c) Huy Pham. All rights reserved.
//  This source code is made available under the terms of the Microsoft Public License (Ms-PL)
//  http://www.opensource.org/licenses/ms-pl.html
//  ---------------------------------

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace ExternalControl.SplitButton
{
    [TemplatePart(Name = "PART_DropButton", Type = typeof(ToggleButton))]
    public class SplitButton : ToggleButton
    {

        #region Dependency Properties

        public static readonly DependencyProperty DropDownContextMenuProperty = DependencyProperty.Register("DropDownContextMenu", typeof(ContextMenu), typeof(SplitButton), new UIPropertyMetadata(null));
        public static readonly DependencyProperty ImageProperty = DependencyProperty.Register("Image", typeof(ImageSource), typeof(SplitButton));
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(SplitButton));
        public static readonly DependencyProperty TargetProperty = DependencyProperty.Register("Target", typeof(UIElement), typeof(SplitButton));
        public static readonly DependencyProperty DropDownButtonCommandProperty = DependencyProperty.Register("DropDownButtonCommand", typeof(ICommand), typeof(SplitButton), new FrameworkPropertyMetadata(null));
        public static readonly DependencyProperty UseToggleProperty = DependencyProperty.Register("UseToggle", typeof(bool), typeof(SplitButton), new FrameworkPropertyMetadata(false));

        #endregion

        #region Constructors

        public SplitButton()
        {
        }

        #endregion

        #region Properties

        public ContextMenu DropDownContextMenu
        {
            get { return GetValue(DropDownContextMenuProperty) as ContextMenu; }
            set { SetValue(DropDownContextMenuProperty, value); }
        }

        public ImageSource Image
        {
            get { return GetValue(ImageProperty) as ImageSource; }
            set { SetValue(ImageProperty, value); }
        }

        public string Text
        {
            get { return GetValue(TextProperty) as string; }
            set { SetValue(TextProperty, value); }
        }

        public UIElement Target
        {
            get { return GetValue(TargetProperty) as UIElement; }
            set { SetValue(TargetProperty, value); }
        }

        public ICommand DropDownButtonCommand
        {
            get { return GetValue(DropDownButtonCommandProperty) as ICommand; }
            set { SetValue(DropDownButtonCommandProperty, value); }
        }

        public bool UseToggle
        {
            get { return (bool)GetValue(UseToggleProperty); }
            set { SetValue(UseToggleProperty, value); }
        }

        #endregion

        #region Public Override Methods

        /// <summary>
        /// 
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetDropButtonCommand();

            var button = Template.FindName("PART_DropButton", this) as ToggleButton;
            button.Click += DropButton_Click;
        }

        void DropButton_Click(object sender, RoutedEventArgs e)
        {
            // FIX: Prevent to open when command is disabled
            if (DropDownButtonCommand != null)
            {
                DropDownButtonCommand.Execute(null);
            }

            // If there is a drop-down assigned to this button, then position and display it 
            DropDownContextMenu.PlacementTarget = this;
            DropDownContextMenu.Placement = PlacementMode.Bottom;
            DropDownContextMenu.IsOpen = !DropDownContextMenu.IsOpen;
        }

        #endregion

        #region Protected Override Methods

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property == DropDownButtonCommandProperty)
                SetDropButtonCommand();

            if (e.Property == DropDownContextMenuProperty)
            {
                NameScope.SetNameScope(DropDownContextMenu, NameScope.GetNameScope(this));
            }
        }

        #endregion

        #region Private Methods

        private void SetDropButtonCommand()
        {
            // Set up the event handlers
            if (Template != null)
            {
                var button = Template.FindName("PART_DropButton", this) as ButtonBase;
                if (button != null)
                {
                    button.Command = DropDownButtonCommand;
                }
            }
        }

        #endregion
    }
}