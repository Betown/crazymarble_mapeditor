﻿using CrazyMarble_MapEditor.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CrazyMarble_MapEditor.View
{
    /// <summary>
    /// EdgeView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class EdgeView : UserControl
    {
        public EdgeView()
        {
            InitializeComponent();
        }

        #region Properties
        public Point StartPoint { get { return new Point(Arrow.X1, Arrow.Y1); } }
        public Point EndPoint { get { return new Point(Arrow.X2, Arrow.Y2); } }
        #endregion

        #region Inner Converters
        public class DragPointConverter : InnerMultiConverter
        {
            public override object Convert(
                object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                double dX = System.Convert.ToDouble(values[5]) - System.Convert.ToDouble(values[3]);
                double dY = System.Convert.ToDouble(values[6]) - System.Convert.ToDouble(values[4]);
                double pos = System.Convert.ToDouble(values[1]) - System.Convert.ToDouble(values[0]) / 4;
                pos *= Math.Cos(System.Convert.ToDouble(Math.PI * System.Convert.ToDouble(values[2]) / 2 / 180.0));
                pos = 1 - pos / Math.Sqrt(dX * dX + dY * dY);

                var left = System.Convert.ToDouble(values[3]) + dX * pos - System.Convert.ToDouble(values[0]) / 2;
                left = double.IsNaN(left) ? 0 : left;
                var top = System.Convert.ToDouble(values[4]) + dY * pos - System.Convert.ToDouble(values[0]) / 2;
                top = double.IsNaN(top) ? 0 : top;
                return new Thickness(left, top, 0, 0);
            }

            public override object[] ConvertBack(
                object value, System.Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return null;
            }
        }
        #endregion

    }

    #region Outer Converters
    public class EdgeViewDragPointConverter : OuterMultiConverter<EdgeView.DragPointConverter>
    { }
    #endregion
}
