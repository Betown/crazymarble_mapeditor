﻿using CrazyMarble_MapEditor.Util;
using CrazyMarble_MapEditor.ViewModel;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CrazyMarble_MapEditor.View
{
    /// <summary>
    /// StatisticsWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class StatisticsWindow : Window
    {
        public StatisticsWindow(MapViewModel map)
        {
            DataContext = map.Statistics;
            map.Statistics.BeforeInitialAutoZoom();
            InitializeComponent();
            Dispatcher.BeginInvoke(DispatcherPriority.Loaded, 
                new Action(() => {
                    SizeToContent = SizeToContent.Manual;
                    map.Statistics.InitialAutoZoom();
                } ));
        }

        #region Inner Converters

        #region Common Converters

        public class ZoomedPositionConverter : InnerMultiConverter
        {
            public override object Convert(
                object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                // 0: Axis Value, 1: Zoomed Size, 2: Original Size
                if (values.Length == 2)
                {
                    double[] doubles = new double[values.Length];
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (UtilFunc.IsNumeric(values[i]))
                        {
                            doubles[i] = System.Convert.ToDouble(values[i]);
                        }
                    }
                    return doubles[0] * doubles[1];
                }

                return 0;
            }

            public override object[] ConvertBack(
                object value, System.Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return null;
            }
        }

        #endregion

        #region Node Representation Converters
        public class NodeIconSourceConverter : InnerConverter
        {
            public override object Convert(
                object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                if (value.GetType() == typeof(string))
                {
                    return string.Format("../Image/Space/Statistics/StatisticsSpace_{0}.png", value);
                }
                else return string.Empty;
            }

            public override object ConvertBack(
                object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return null;
            }
        }
        #endregion

        #region Edge Representation Converters
        public class ZoomedEdgePointConverterBase : InnerMultiConverter
        {
            private bool _isX;
            private bool _isEnd;

            public ZoomedEdgePointConverterBase(bool isX, bool isEnd)
                : base()
            {
                _isX = isX;
                _isEnd = isEnd;
            }

            public override object Convert(
                object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                // 0: X/Y axis, 1: Original Width/Height, 2: Reduced Width/Height, 3: Angle, 4:Zoom
                if (values.Length == 5)
                {
                    double[] doubles = new double[values.Length];
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (UtilFunc.IsNumeric(values[i]))
                        {
                            doubles[i] = System.Convert.ToDouble(values[i]);
                        }
                    }

                    var diff = doubles[1] * doubles[4] - doubles[2];
                    double extraPos = 0;
                    if (_isX)
                    {
                        extraPos = (diff / 2) * Math.Cos(doubles[3]);
                    }
                    else
                    {
                        extraPos = (diff / 2) * -Math.Sin(doubles[3]);
                    }

                    double beforeExtend = doubles[0] * doubles[4];
                    if (_isEnd)
                    {
                        return beforeExtend + extraPos;
                    }
                    else
                    {
                        return beforeExtend - extraPos;
                    }
                }

                throw new ArgumentException("Values must be 5 numerics.");
            }

            public override object[] ConvertBack(
                object value, System.Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return null;
            }
        }
        public class ZoomedBeginXConverter : ZoomedEdgePointConverterBase
        {
            public ZoomedBeginXConverter() : base(true, false) { }
        }
        public class ZoomedBeginYConverter : ZoomedEdgePointConverterBase
        {
            public ZoomedBeginYConverter() : base(false, false) { }
        }
        public class ZoomedEndXConverter : ZoomedEdgePointConverterBase
        {
            public ZoomedEndXConverter() : base(true, true) { }
        }
        public class ZoomedEndYConverter : ZoomedEdgePointConverterBase
        {
            public ZoomedEndYConverter() : base(false, true) { }
        }
        #endregion

        #endregion

        #region Dependency Property
        // Dependency Property
        public static readonly DependencyProperty ZoomedNodeWidthProperty =
             DependencyProperty.Register("ZoomedNodeWidth",
             typeof(double),
             typeof(StatisticsWindow));
        // .NET Property wrapper
        public double ZoomedNodeWidth
        {
            get { return (double)GetValue(ZoomedNodeWidthProperty); }
            set { SetCurrentValue(ZoomedNodeWidthProperty, value); }
        }
        // Dependency Property
        public static readonly DependencyProperty ZoomedNodeHeightProperty =
             DependencyProperty.Register("ZoomedNodeHeight",
             typeof(double),
             typeof(StatisticsWindow));

        // .NET Property wrapper
        public double ZoomedNodeHeight
        {
            get { return (double)GetValue(ZoomedNodeHeightProperty); }
            set { SetCurrentValue(ZoomedNodeHeightProperty, value); }
        }
        #endregion
    }

    #region Outer Converters
    public class StatisticsZoomedPositionConverter :
        OuterMultiConverter<StatisticsWindow.ZoomedPositionConverter>
    { }
    public class StatisticsNodeIconSourceConverter :
        OuterConverter<StatisticsWindow.NodeIconSourceConverter>
    { }

    public class StatisticsEdgeBeginXConverter :
        OuterMultiConverter<StatisticsWindow.ZoomedBeginXConverter>
    { }
    public class StatisticsEdgeBeginYConverter :
        OuterMultiConverter<StatisticsWindow.ZoomedBeginYConverter>
    { }
    public class StatisticsEdgeEndXConverter :
        OuterMultiConverter<StatisticsWindow.ZoomedEndXConverter>
    { }
    public class StatisticsEdgeEndYConverter :
        OuterMultiConverter<StatisticsWindow.ZoomedEndYConverter>
    { }
    #endregion
}
