﻿using CrazyMarble_MapEditor.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CrazyMarble_MapEditor.View
{
    /// <summary>
    /// NodeSpawnPoint.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class NodeSpawnPoint : UserControl
    {
        public NodeSpawnPoint()
        {
            InitializeComponent();
        }

        #region User-Friendly Logics
        private void UserControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Focus();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Focus();
        }
        #endregion

        #region Inner Converters
        public class RealSizeConverter : InnerMultiConverter
        {
            public override object Convert(
                object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                if (values[0].GetType() == typeof(Thickness) &&
                    values[1].GetType() == typeof(double) &&
                    values[2].GetType() == typeof(double))
                {
                    var margin = (Thickness)values[0];
                    return new Size((double)values[1], margin.Bottom + (double)values[2]);
                }
                else return new Size();
            }

            public override object[] ConvertBack(
                object value, System.Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return null;
            }
        }

        public class EffectiveSizeConverter : InnerMultiConverter
        {
            public override object Convert(
                object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                if (values[0].GetType() == typeof(double) &&
                    values[1].GetType() == typeof(double))
                {
                    return new Size((double)values[0] / 2, (double)values[1] / 2);
                }
                else return new Size();
            }

            public override object[] ConvertBack(
                object value, System.Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return null;
            }
        }
        #endregion
    }

    #region Outer Converters
    public class SpawnPointRealSizeConverter : 
        OuterMultiConverter<NodeSpawnPoint.RealSizeConverter>
    { }

    public class SpawnPointEffectiveSizeConverter :
        OuterMultiConverter<NodeSpawnPoint.EffectiveSizeConverter>
    { }
    #endregion

}
