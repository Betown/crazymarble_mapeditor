﻿using CrazyMarble_MapEditor.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CrazyMarble_MapEditor.View
{
    /// <summary>
    /// NodeView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class NodeView : UserControl
    {
        private static double innerProportion = .88;

        public NodeView()
        {
            InitializeComponent();
        }

        #region User-Friendly Logics
        private void UserControl_MouseEnter(object sender, MouseEventArgs e)
        {
            CircularBorder.Visibility = Visibility.Visible;
        }
        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!IsFocused) CircularBorder.Visibility = Visibility.Hidden;
        }

        private void UserControl_GotFocus(object sender, RoutedEventArgs e)
        {
            CircularBorder.Visibility = Visibility.Visible;
        }
        private void UserControl_LostFocus(object sender, RoutedEventArgs e)
        {
            CircularBorder.Visibility = Visibility.Hidden;
        }

        private void UserControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Focus();
        }
        #endregion

        #region Inner Converters
        public class InnerCircleDiameterConverter : InnerConverter
        {
            public override object Convert(
                object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                if (value.GetType() == typeof(double)) return ((double)value) * NodeView.innerProportion;
                else return 0;
            }

            public override object ConvertBack(
                object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return null;
            }
        }

        public class InnerCircleMarginConverter : InnerMultiConverter
        {
            public override object Convert(
                object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                if (values.Length == 2)
                {
                    double[] doubles = new double[values.Length];
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (UtilFunc.IsNumeric(values[i]))
                        {
                            var dbl = System.Convert.ToDouble(values[i]);
                            if (!double.IsNaN(dbl))
                            {
                                doubles[i] = dbl;
                            }
                        }
                    }

                    var marginProportion = (1 - NodeView.innerProportion) / 2;
                    return new Thickness(
                        doubles[0] * marginProportion, doubles[1] * marginProportion, 0, 0);
                }
                else return new Thickness();
            }

            public override object[] ConvertBack(
                object value, System.Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return null;
            }
        }

        public class RealSizeConverter : InnerMultiConverter
        {
            public override object Convert(
                object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                if (values.Length == 3)
                {
                    double[] doubles = new double[values.Length];
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (UtilFunc.IsNumeric(values[i]))
                        {
                            doubles[i] = System.Convert.ToDouble(values[i]);
                        }
                    }

                    return new Size(doubles[0] + doubles[2] * 2, doubles[1] + doubles[2] * 2);
                }
                else return new Size();
            }

            public override object[] ConvertBack(
                object value, System.Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return null;
            }
        }
        #endregion

        #region Dependency Property
        // Dependency Property
        public static readonly DependencyProperty IsSelectedProperty =
             DependencyProperty.Register("IsSelected",
             typeof(bool),
             typeof(NodeView),
             new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsArrange,
             new PropertyChangedCallback(OnSelected)));

        // .NET Property wrapper
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        private static void OnSelected(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var originator = obj as NodeView;
            if (originator != null)
            {
                originator.Focus();
            }
        }
        #endregion
    }

    #region Outer Converters
    public class InnerCircleDiameterConverter : 
        OuterConverter<NodeView.InnerCircleDiameterConverter>
    { }

    public class InnerCircleMarginConverter : 
        OuterMultiConverter<NodeView.InnerCircleMarginConverter>
    { }

    public class NodeRealSizeConverter : 
        OuterMultiConverter<NodeView.RealSizeConverter>
    { }
    #endregion
}
