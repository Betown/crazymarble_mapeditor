﻿using CrazyMarble_MapEditor.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CrazyMarble_MapEditor.Model
{
    #region Map Characterestics
    public class Characteristics
    {
        private readonly static Dictionary<string, CharacteristicsElement> _base;
        private readonly Dictionary<string, CharacteristicsElement> _values;
        static Characteristics()
        {
            _base = new Dictionary<string, CharacteristicsElement>();
            _base.Add("Money", new CharacteristicsElement(Resources.CharactersiticsMoney));
            _base.Add("Draw", new CharacteristicsElement(Resources.CharactersiticsDraw));
            _base.Add("Randomness", new CharacteristicsElement(Resources.CharactersiticsRandomness));
            _base.Add("Complexity", new CharacteristicsElement(Resources.CharactersiticsComplexity));
        }

        public Characteristics()
        {
            _values = new Dictionary<string, CharacteristicsElement>(_base);
        }

        public ICollection<CharacteristicsElement> Values { get { return _values.Values; } }

        public int Money 
        {
            get { return _values["Money"].Value; }
            set { _values["Money"].Value = value; } 
        }
        public int Draw
        {
            get { return _values["Draw"].Value; }
            set { _values["Draw"].Value = value; }
        }
        public int Randomness
        {
            get { return _values["Randomness"].Value; }
            set { _values["Randomness"].Value = value; }
        }
        public int Complexity
        {
            get { return _values["Complexity"].Value; }
            set { _values["Complexity"].Value = value; }
        }

        public class CharacteristicsElement
        {
            public CharacteristicsElement(string name)
            {
                Name = name;
                Value = 0;
            }

            private int _value;
            private string _scoreImage;
            public int Value
            {
                get { return _value; }
                set 
                { 
                    _value = Math.Min(100, Math.Max(0, value));
                    ScoreImage = string.Format("../Image/Statistics/StatisticsCharacteristics_Score{0}.png", Value / 10);
                }
            }
            public string ScoreImage
            {
                get { return _scoreImage; }
                private set { _scoreImage = value; }
            }
            public string Name { get; set; }
        }
    }
    #endregion

    #region Statistics Legends
    public class StatisticsLegendElement
    {
        public StatisticsLegendElement()
        { }
        public StatisticsLegendElement(StatisticsLegendElement copy)
        {
            IconSource = String.Copy(copy.IconSource ?? string.Empty);
            Name = String.Copy(copy.Name ?? string.Empty);
            Count = copy.Count;
            PortionString = String.Copy(copy.PortionString ?? string.Empty);
            Color = copy.Color ?? Brushes.Black;
        }
        double _portion;

        public double Portion
        {
            get { return _portion; }
            set 
            {
                _portion = Math.Min(100, Math.Max(0, value * 100.0));
                if (double.IsNaN(value))
                {
                    PortionString = string.Format("({0})", Resources.ValueNotAvailable);
                }
                else
                {
                    PortionString = string.Format("({0:0.0%})", value);
                }
            }
        }
        // Node Statistics Only.
        public string IconSource { get; set; }
        // Edge Statistics Only.
        public Brush Color { get; set; }

        public string Name { get; set; }
        public int Count { get; set; }
        public string PortionString { get; set; }
    }
    #endregion
}
