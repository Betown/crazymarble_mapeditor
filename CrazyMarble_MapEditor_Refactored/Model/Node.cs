using CrazyMarble_MapEditor.Properties;
using CrazyMarble_MapEditor.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrazyMarble_MapEditor.Model
{
    public class Node
    {
        #region Constructor / Copy
        public Node()
        {
            PrevList = new HashSet<Integer>();
            Position = new Point(0, 0);
        }

        public Node(Node source) : this()
        {
            // Properties to copy
            Position = new Point(source.Position.X, source.Position.Y);
            ID = new Integer(source.ID.Value);
            InitialLevel = source.InitialLevel;
            Type = source.Type;

            Next = source.Next;
            NextIfArrived = source.NextIfArrived;
            JumpTo = source.JumpTo;
        }
        #endregion

        #region Members
        [JsonProperty("Type")]
        private SpaceType _type;
        #endregion

        #region Properties
        public Integer ID { get; set; }

        public int InitialLevel { get; set; }

        [JsonIgnore]
        public SpaceType Type {
            get { return _type; }
            set {
                if (_type != null)
                {
                    _coerceInitialLevel(value);
                }
                _type = value;
            } 
        }
        [JsonIgnore]
        public int MaxLevel
        {
            get { return SpaceType.MaxLevel(Type); }
        }

        private void _coerceInitialLevel(SpaceType newType)
        {
            InitialLevel = Math.Min(InitialLevel, SpaceType.MaxLevel(newType));
        }
        public int TryCoerceInitialLevel(SpaceType newType)
        {
            return Math.Min(InitialLevel, SpaceType.MaxLevel(newType));
        }

        public Point Position { get; set; }

        public Integer Next { get; set; }
        public Integer NextIfArrived { get; set; }
        public Integer JumpTo { get; set; }

        [JsonIgnore] // Since list of previous list will be reconstructed 
        public HashSet<Integer> PrevList { get; set; }

        public static ICollection<string> SpaceTypes
        {
            get { return SpaceType.Values; }
        }
        public static ICollection<string> SpaceTypeNames
        {
            get { return SpaceType.ValueNames; }
        }
        #endregion

        #region SpaceType(Enum)
        [JsonConverter(typeof(SpaceType.SpaceTypeConverter))]
        public sealed class SpaceType
        {
            private readonly int _value;
            private readonly String _originalName;
            private readonly String _UIname;
            private readonly int _maxLevel;

            private static readonly Dictionary<string, SpaceType> instance = 
                new Dictionary<string, SpaceType>();
            private static readonly Dictionary<string, SpaceType> instanceOriginal = 
                new Dictionary<string, SpaceType>();

            public static readonly SpaceType Property;// = new SpaceType(0, "Property", "소유지", 5);
            public static readonly SpaceType Chest;// = new SpaceType(1, "Chest", "상자", 3);
            public static readonly SpaceType Event;// = new SpaceType(2, "Event", "이벤트", 1);
            public static readonly SpaceType Start;// = new SpaceType(3, "Start", "시작점", 1);
            public static readonly SpaceType Shop;// = new SpaceType(4, "Shop", "상점", 1);
            public static readonly SpaceType Warp;// = new SpaceType(5, "Warp", "워프", 1);
            public static readonly SpaceType HouseOfSage;// = new SpaceType(6, "HouseOfSage", "현자의 집", 1);
            public static readonly SpaceType Bank;// = new SpaceType(7, "Bank","은행", 1);

            #region Initializer & Constructor
            static SpaceType()
            {
                Property = new SpaceType(0, "Property", Resources.SpaceTypeProperty, 5);
                Chest = new SpaceType(1, "Chest", Resources.SpaceTypeChest, 3);
                Event = new SpaceType(2, "Event", Resources.SpaceTypeEvent, 1);
                Start = new SpaceType(3, "Start", Resources.SpaceTypeStart, 1);
                Shop = new SpaceType(4, "Shop", Resources.SpaceTypeShop, 1);
                Warp = new SpaceType(5, "Warp", Resources.SpaceTypeWarp, 1);
                HouseOfSage = new SpaceType(6, "HouseOfSage", Resources.SpaceTypeHouseOfSage, 1);
                Bank = new SpaceType(7, "Bank", Resources.SpaceTypeBank, 1);

                _addInstance(Property);
                _addInstance(Chest);
                _addInstance(Event);
                _addInstance(Start);
                _addInstance(Shop);
                _addInstance(Warp);
                _addInstance(HouseOfSage);
                _addInstance(Bank);
            }
            private static void _addInstance(SpaceType type)
            {
                instance[type._UIname] = type;
                instanceOriginal[type._originalName] = type;
            }

            internal static ICollection<string> Values
            {
                get { return instance.Keys; }
            }
            internal static ICollection<string> ValueNames
            {
                get { return instanceOriginal.Keys; }
            }

            private SpaceType(int value, String OriginalName, String UIname, int maxLevel)
            {
                this._UIname = UIname;
                this._originalName = OriginalName;
                this._value = value;
                this._maxLevel = maxLevel;
            }
            #endregion

            #region Property Helpers
            public static int MaxLevel(SpaceType type)
            {
                return type._maxLevel;
            }
            #endregion

            #region Conversion Methods
            public override String ToString()
            {
                return _UIname;
            }
            public static SpaceType Parse(string type)
            {
                SpaceType result;
                if (instanceOriginal.TryGetValue(type, out result))
                    return result;
                else
                    throw new ArgumentException("Given string \"" + type + "\" is not correct.");
            }
            #endregion

            #region Implicit Conversions
            public static implicit operator SpaceType(string str)
            {
                SpaceType result;
                if (instance.TryGetValue(str, out result))
                    return result;
                else
                    throw new InvalidCastException("Given string \"" + str + "\" is not correct.");
            }

            public static implicit operator int(SpaceType type)
            {
                return type._value;
            }

            public static string ToOriginalName(SpaceType type)
            {
                return type._originalName;
            }
            public static string GetName(SpaceType type)
            {
                return type._UIname;
            }
            #endregion

            #region Json Converter
            public class SpaceTypeConverter : JsonConverter
            {
                public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
                {
                    JToken t = JToken.FromObject((value as SpaceType)._originalName);
                    t.WriteTo(writer);
                }

                public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
                {
                    return SpaceType.Parse(serializer.Deserialize<string>(reader));
                }

                public override bool CanRead
                {
                    get { return true; }
                }

                public override bool CanConvert(Type objectType)
                {
                    return objectType == typeof(SpaceType);
                }
            }
            #endregion
        }
        #endregion

        #region Position(Point)
        public class Point
        {
            private Integer _X, _Y;

            public Point(int X, int Y)
            {
                _X = X; _Y = Y;
            }

            public Integer X
            {
                get { return _X; }
                set { _X = value; }
            }
            public Integer Y
            {
                get { return _Y; }
                set { _Y = value; }
            }

            public override string ToString()
            {
                return "( " + _X + ", " + _Y + " )";
            }
        }
        #endregion
    }
}
