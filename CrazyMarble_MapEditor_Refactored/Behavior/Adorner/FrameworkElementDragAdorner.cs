﻿using CrazyMarble_MapEditor.Util;
using CrazyMarble_MapEditor.View;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace CrazyMarble_MapEditor.Behavior
{
    class FrameworkElementDragAdorner : Adorner
    {
        private ToolTip _tooltip;
        private Point _startPoint;

        // Be sure to call the base class constructor.
        public FrameworkElementDragAdorner(UIElement adornedElement, Point startPoint, double zoom)
            : base(adornedElement)
        {
            VisualBrush _brush = new VisualBrush(adornedElement);
            _brush.Stretch = Stretch.None;

            _startPoint = startPoint;
            _zoom = zoom;

            _child = new Rectangle();
            var size = (Size)adornedElement.GetValue(AttachedProperties.RealSizeProperty);
            if (size.Width > 0 && size.Height > 0)
            {
                _child.Width = size.Width;
                _child.Height = size.Height;
            }
            else
            {
                _child.Width = adornedElement.RenderSize.Width;
                _child.Height = adornedElement.RenderSize.Height;
            }

            DoubleAnimation animation = new DoubleAnimation(0.5, 1, new Duration(TimeSpan.FromSeconds(1)));
            animation.AutoReverse = true;
            animation.RepeatBehavior = System.Windows.Media.Animation.RepeatBehavior.Forever;
            _brush.BeginAnimation(System.Windows.Media.Brush.OpacityProperty, animation);

            _child.Fill = _brush;

            _tooltip = new ToolTip();
            _tooltip.Placement = System.Windows.Controls.Primitives.PlacementMode.RelativePoint;
            _tooltip.HorizontalOffset = 100;
            _tooltip.VerticalOffset = 100;
            _tooltip.PlacementTarget = this;
            _tooltip.Content = "( " + VisualTreeHelper.GetOffset(adornedElement).X + 
                ", " + VisualTreeHelper.GetOffset(adornedElement).Y + " )";
            _tooltip.IsOpen = true;

            IsVisibleChanged += visibleChanged;
        }

        protected override Size MeasureOverride(Size constraint)
        {
            _child.Measure(constraint);
            return _child.DesiredSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            _child.Arrange(new Rect(finalSize));
            return finalSize;
        }

        protected override Visual GetVisualChild(int index)
        {
            return _child;
        }

        protected override int VisualChildrenCount
        {
            get
            {
                return 1;
            }
        }

        public double LeftOffset
        {
            get
            {
                return _leftOffset;
            }
            set
            {
                _leftOffset = value;
                UpdatePosition();
            }
        }

        public double TopOffset
        {
            get
            {
                return _topOffset;
            }
            set
            {
                _topOffset = value;
                UpdatePosition();

            }
        }

        public double Zoom
        {
            get 
            { 
                return _zoom; 
            }
            set 
            { 
                _zoom = value;
                UpdatePosition();
            }
        }
        

        private void UpdatePosition()
        {
            AdornerLayer adornerLayer = this.Parent as AdornerLayer;
            if (adornerLayer != null)
            {
                adornerLayer.Update(AdornedElement);
                _tooltip.Placement = System.Windows.Controls.Primitives.PlacementMode.Absolute;
                _tooltip.Placement = System.Windows.Controls.Primitives.PlacementMode.RelativePoint;
                _tooltip.Content = "( " + (int)(_startPoint.X + _leftOffset) +  ", " 
                    + (int)(_startPoint.Y + _topOffset) + " )";
            }
        }

        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            GeneralTransformGroup result = new GeneralTransformGroup();
            result.Children.Add(base.GetDesiredTransform(transform));
            result.Children.Add(new TranslateTransform((int)(_leftOffset * _zoom), (int)(_topOffset * _zoom)));
            return result;
        }

        private void visibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(!IsVisible) _tooltip.IsOpen = false;
        }

        private Rectangle _child = null;
        private double _leftOffset = 0;
        private double _topOffset = 0;
        private double _zoom;
    }
}
