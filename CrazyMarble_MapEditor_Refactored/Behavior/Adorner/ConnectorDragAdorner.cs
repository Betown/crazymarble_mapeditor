﻿using CrazyMarble_MapEditor.View;
using Petzold.Media2D;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace CrazyMarble_MapEditor.Behavior
{
    class ConnectorDragAdorner : Adorner
    {
        // Be sure to call the base class constructor.
        public ConnectorDragAdorner(UIElement adornedElement, Point startPoint)
            : base(adornedElement)
        {
            if (adornedElement is EdgeView)
            {
                var edge = adornedElement as EdgeView;
                _child = new ArrowLine();

                // 시작 지점 결정하기
                _child.X1 = startPoint.X;
                _child.Y1 = startPoint.Y;
                _child.X2 = _child.X1;
                _child.Y2 = _child.Y1;

                // 선 굵기/색상/애니메이션
                _child.ArrowAngle = 55;
                Brush _brush = edge.Arrow.Stroke.Clone();
                DoubleAnimation animation = new DoubleAnimation(0.3, 1, new Duration(TimeSpan.FromSeconds(1)));
                animation.AutoReverse = true;
                animation.RepeatBehavior = System.Windows.Media.Animation.RepeatBehavior.Forever;
                _brush.BeginAnimation(System.Windows.Media.Brush.OpacityProperty, animation);

                _child.Stroke = _brush;
                _child.StrokeThickness = 3;

            }
            else
            {
                throw new NotImplementedException("This accepts only NodeConnector element");
            }
        }

        protected override Size MeasureOverride(Size constraint)
        {
            _child.Measure(constraint);
            return _child.DesiredSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            _child.Arrange(new Rect(finalSize));
            return finalSize;
        }

        protected override Visual GetVisualChild(int index)
        {
            return _child;
        }

        protected override int VisualChildrenCount
        {
            get
            {
                return 1;
            }
        }

        public double LeftOffset
        {
            get
            {
                return _leftOffset;
            }
            set
            {
                _leftOffset = value;
                UpdatePosition();
            }
        }

        public double TopOffset
        {
            get
            {
                return _topOffset;
            }
            set
            {
                _topOffset = value;
                UpdatePosition();

            }
        }

        private void UpdatePosition()
        {
            AdornerLayer adornerLayer = this.Parent as AdornerLayer;
            if (adornerLayer != null)
            {
                _child.X2 = _child.X1 + _leftOffset;
                _child.Y2 = _child.Y1 + _topOffset;
                adornerLayer.Update(AdornedElement);
            }
        }

        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            GeneralTransformGroup result = new GeneralTransformGroup();
            result.Children.Add(base.GetDesiredTransform(transform));
            return result;
        }

        private ArrowLine _child = null;
        private double _leftOffset = 0;
        private double _topOffset = 0;
    }
}
