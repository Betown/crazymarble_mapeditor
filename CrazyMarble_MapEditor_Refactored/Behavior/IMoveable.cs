﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CrazyMarble_MapEditor.Behavior
{
    public interface IMoveable
    {
        double X { get; }
        double Y { get; }

        double Width { get; }
        double Height { get; }

        /// <summary>
        /// Type of the data item
        /// </summary>
        Type DataType { get; }

        /// <summary>
        /// Start movement of data item
        /// </summary>
        void OnMoveStart();

        /// <summary>
        /// On Move Finished, with displacement
        /// </summary>
        void Move(object target, Vector displacement);
    }

    public interface IMovePanel
    {
        /// <summary>
        /// Type of the data item
        /// </summary>
        Type DataType { get; }

        /// <summary>
        /// Finish element movement in the collection.
        /// </summary>
        /// <param name="data">The data to be dropped</param>
        /// <param name="displacement">Displacement from former position</param>
        void MoveEnd(object data, Vector displacement);
    }
}
