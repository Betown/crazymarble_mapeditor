﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace CrazyMarble_MapEditor.Behavior
{
    class FrameworkElementConnectableBehavior : Behavior<FrameworkElement>
    {
        private Type connectableType;
        private Type connectableConnectorType;

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.DragEnter += AssociatedObject_DragEnter;
            this.AssociatedObject.DragOver += AssociatedObject_DragOver;
            this.AssociatedObject.DragLeave += AssociatedObject_DragLeave;
            this.AssociatedObject.Drop += AssociatedObject_Drop;

        }

        void AssociatedObject_Drop(object sender, DragEventArgs e)
        {
            if (connectableType != null && connectableConnectorType != null)
            {
                //if the data type can be dropped 
                if (e.Data.GetDataPresent(typeof(IConnector)) && 
                    e.Data.GetDataPresent(typeof(Vector)))
                {
                    var connector = e.Data.GetData(typeof(IConnector)) as IConnector;

                    // Try Connect
                    if (connector != null)
                    {
                        if (connectableConnectorType.IsInstanceOfType(connector) &&
                            connectableType.IsInstanceOfType(connector.Start))
                        {
                            IConnectable connected = connector.Start;
                            IConnectable connecting = this.AssociatedObject.DataContext as IConnectable;

                            if (connected.IsConnectValid(connector, connecting))
                            {
                                connector.Connect(connecting);
                            }
                            else
                            {
                                connector.OnConnectFailed((Vector)e.Data.GetData(typeof(Vector)));
                            }
                        }
                        else
                        {
                            connector.OnConnectFailed((Vector)e.Data.GetData(typeof(Vector)));
                        }

                    }
                }
            }

            e.Handled = true;
            return;
        }

        void AssociatedObject_DragLeave(object sender, DragEventArgs e)
        {
            e.Handled = true;
        }

        void AssociatedObject_DragOver(object sender, DragEventArgs e)
        {
            if (connectableType == null || connectableConnectorType == null)
            {
                //if item can be dropped
                if (e.Data.GetDataPresent(typeof(IConnectable)))
                {
                    //if the data type can be dropped 
                    if (e.Data.GetDataPresent(typeof(IConnectable)))
                    {
                        e.Effects = DragDropEffects.Copy;
                    }
                    else
                    {
                        e.Effects = DragDropEffects.None;  //default to None
                    }
                }
            }
            e.Handled = true;
        }

        void AssociatedObject_DragEnter(object sender, DragEventArgs e)
        {
            //if the DataContext implements IDropable, record the data type that can be dropped
            if (connectableType == null || connectableConnectorType == null)
            {
                if (this.AssociatedObject.DataContext != null)
                {
                    IConnectable connectable = this.AssociatedObject.DataContext as IConnectable;
                    if (connectable != null)
                    {
                        this.connectableType = connectable.ConnectableType;
                        this.connectableConnectorType = connectable.ConnectableConnectorType;
                    }
                }
            }
            e.Handled = true;
        }
    }
}
