﻿using CrazyMarble_MapEditor.View;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace CrazyMarble_MapEditor.Behavior
{
    // Datacontext MUST CONTAIN X, Y property.
    class ConnectorDragBehavior : Behavior<EdgeView>
    {
        private bool _isMouseDown;
        private bool _isDragging;

        private Point _startPoint;
        private double _leftOffset = 0;
        private double _topOffset = 0;
        private ConnectorDragAdorner _overlayElement;

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.MouseLeftButtonDown += AssociatedObject_MouseLeftButtonDown;
            this.AssociatedObject.MouseMove += AssociatedObject_MouseMove;
            this.AssociatedObject.MouseLeftButtonUp += AssociatedObject_MouseLeftButtonUp;

        }

        void AssociatedObject_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _isMouseDown = true;
            _startPoint = e.GetPosition(Container);
            AssociatedObject.CaptureMouse();
            e.Handled = true;
        }

        void AssociatedObject_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isMouseDown)
            {
                // 드래그 시작
                if ((!_isDragging) &&
                    ((Math.Abs(e.GetPosition(Container).X - _startPoint.X) > SystemParameters.MinimumHorizontalDragDistance) ||
                    (Math.Abs(e.GetPosition(Container).Y - _startPoint.Y) > SystemParameters.MinimumVerticalDragDistance)))
                {
                    _isDragging = true;

                    var edge = AssociatedObject as EdgeView;
                    var X = edge.StartPoint.X < edge.EndPoint.X ? 0 : AssociatedObject.ActualWidth;
                    var Y = edge.StartPoint.Y < edge.EndPoint.Y ? 0 : AssociatedObject.ActualHeight;
                    _startPoint = AssociatedObject.TranslatePoint(new Point(X, Y), Container);
                    AssociatedObject.Opacity = .5;

                    _overlayElement = new ConnectorDragAdorner(AssociatedObject, new Point(X, Y));
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(AssociatedObject);
                    layer.Add(_overlayElement);
                }
                if (_isDragging)
                {
                    Point currentPosition = System.Windows.Input.Mouse.GetPosition(Container);

                    if (Container != null)
                    {
                        var snapX = Math.Max(currentPosition.X, 0);
                        var snapY = Math.Max(currentPosition.Y, 0);
                        snapX = Math.Min(snapX, Container.ActualWidth);
                        snapY = Math.Min(snapY, Container.ActualHeight);

                        currentPosition = new Point(snapX, snapY);
                    }

                    _overlayElement.LeftOffset = currentPosition.X - _startPoint.X;
                    _overlayElement.TopOffset = currentPosition.Y - _startPoint.Y;
                    _leftOffset = currentPosition.X - _startPoint.X;
                    _topOffset = currentPosition.Y - _startPoint.Y;
                }
            }
        }

        void AssociatedObject_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_isMouseDown)
            {
                System.Windows.Input.Mouse.Capture(null);
                if (_isDragging)
                {
                    AdornerLayer.GetAdornerLayer(AssociatedObject).Remove(_overlayElement);
                    _overlayElement = null;

                    AssociatedObject.Opacity = 1;

                    //set the item's DataContext as the data to be transferred
                    IConnector dragObject = this.AssociatedObject.DataContext as IConnector;
                    if (dragObject != null)
                    {
                        DataObject data = new DataObject();
                        data.SetData(typeof(IConnector), this.AssociatedObject.DataContext);
                        data.SetData(typeof(Vector), new Vector(_leftOffset, _topOffset));
                        var ret = System.Windows.DragDrop.DoDragDrop(this.AssociatedObject, data, DragDropEffects.Copy);
                    }
                    e.Handled = true;
                }
                _isDragging = false;
                AssociatedObject.Focus();
            }
            _isMouseDown = false;
        }

        #region Dependency Property
        // Dependency Property
        public static readonly DependencyProperty ContainerProperty =
             DependencyProperty.Register("Container", 
             typeof(FrameworkElement),
             typeof(ConnectorDragBehavior), 
             new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange));

        // .NET Property wrapper
        public FrameworkElement Container
        {
            get { return (FrameworkElement)GetValue(ContainerProperty); }
            set { SetValue(ContainerProperty, value); }
        }
        #endregion
    }
}
