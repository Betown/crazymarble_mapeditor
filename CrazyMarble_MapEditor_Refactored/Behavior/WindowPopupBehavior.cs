﻿using CrazyMarble_MapEditor.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Threading;

namespace CrazyMarble_MapEditor.Behavior
{
    class WindowPopupBehavior : Behavior<MainWindow>
    {
        private bool _holdingBack;
        private bool _hidingBeyondScroll;
        private MapViewModel _map;

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Activated += AssociatedObject_Activated;
            AssociatedObject.Deactivated += AssociatedObject_Deactivated;
            AssociatedObject.SizeChanged += AssociatedObject_SizeChanged;
            AssociatedObject.StateChanged += AssociatedObject_StateChanged;
            AssociatedObject.LocationChanged += AssociatedObject_LocationChanged;

            _map = AssociatedObject.DataContext as MapViewModel;
        }

        private void _hideNodePropertiesWindow()
        {
            if (_map.IsNodePropertiesWindowOpen)
            {
                _holdingBack = true;
                _map.IsNodePropertiesWindowOpen = false;
            }
        }
        private void _showNodePropertiesWindow()
        {
            if (_holdingBack && !_hidingBeyondScroll)
            {
                _holdingBack = false;
                _map.IsNodePropertiesWindowOpen = true;
            }
        }
        private void _updateNodePropertiesWindow()
        {
            if (_map.IsNodePropertiesWindowOpen)
            {
                _map.UpdateNodePropertiesWindow();
            }
        }

        void AssociatedObject_Activated(object sender, EventArgs e)
        {
            _showNodePropertiesWindow();
        }
        void AssociatedObject_Deactivated(object sender, EventArgs e)
        {
            _hideNodePropertiesWindow();
        }

        void AssociatedObject_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_map != null)
            {
                _updateNodePropertiesWindow();
            }
        }
        void AssociatedObject_StateChanged(object sender, EventArgs e)
        {
            if (_map != null)
            {
                if (AssociatedObject.WindowState == WindowState.Minimized)
                {
                    _hideNodePropertiesWindow();
                }
                else 
                {
                    _showNodePropertiesWindow();
                    _updateNodePropertiesWindow();
                }
            }
        }
        void AssociatedObject_LocationChanged(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke((Action)(() =>
            {
                if (AssociatedObject.WindowState != WindowState.Minimized)
                {
                    if (_map != null)
                    {
                        _updateNodePropertiesWindow();
                    }
                }
            }), DispatcherPriority.Normal);
        }

        void Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (_map != null && _map.FeaturedNode != null)
            {
                var scroll = sender as ScrollViewer;
                if (e.HorizontalChange == 0 && e.VerticalChange == 0 && 
                    e.ExtentWidthChange == 0 && e.ExtentHeightChange == 0)
                {
                    return;
                }

                scrollHideCheck(new Rect(
                    scroll.HorizontalOffset, scroll.VerticalOffset,
                    scroll.ViewportWidth, scroll.ViewportHeight));
            }
        }

        private void Scroll_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_map != null && _map.FeaturedNode != null)
            {
                var scroll = sender as ScrollViewer;
                scrollHideCheck(new Rect(
                    scroll.HorizontalOffset, scroll.VerticalOffset,
                    scroll.ActualWidth, scroll.ActualHeight));
            }
        }

        private void scrollHideCheck(Rect viewport)
        {
            var elem = _map.FeaturedNode as IConnectable;
            var elemRect = new Rect(elem.X * Zoom, elem.Y * Zoom,
                elem.Width * Zoom, elem.Height * Zoom);
            
            var inter = Rect.Intersect(elemRect, viewport);
            if (Math.Round(inter.Width) == Math.Round(elemRect.Width) &&
                Math.Round(inter.Height) == Math.Round(elemRect.Height))
            {
                if (_hidingBeyondScroll)
                {
                    _map.IsNodePropertiesWindowOpen = true;
                    _hidingBeyondScroll = false;
                }
            }
            else
            {
                if (_map.IsNodePropertiesWindowOpen)
                {
                    _map.IsNodePropertiesWindowOpen = false;
                    _hidingBeyondScroll = true;
                }
            }
            _updateNodePropertiesWindow();
        }

        #region Dependency Property
        // Dependency Property
        public static readonly DependencyProperty ScrollProperty =
             DependencyProperty.Register("Scroll",
             typeof(ScrollViewer),
             typeof(WindowPopupBehavior),
             new FrameworkPropertyMetadata(
                 null,
                 FrameworkPropertyMetadataOptions.AffectsArrange,
                 new PropertyChangedCallback(OnScrollChanged)));
        // .NET Property wrapper
        public ScrollViewer Scroll
        {
            get { return (ScrollViewer)GetValue(ScrollProperty); }
            set {
                SetValue(ScrollProperty, value);
            }
        }
        private static void OnScrollChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e){
            var originator = obj as WindowPopupBehavior;
            if (originator != null)
            {
                originator._onScrollChanged(e);
            }
        }
        private void _onScrollChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != null)
            {
                var scroll = e.OldValue as ScrollViewer;
                scroll.ScrollChanged -= Scroll_ScrollChanged;
                Scroll.SizeChanged -= Scroll_SizeChanged;
            }

            if (Scroll != null)
            {
                Scroll.ScrollChanged += Scroll_ScrollChanged;
                Scroll.SizeChanged += Scroll_SizeChanged;
            }
        }

        // Dependency Property
        public static readonly DependencyProperty ZoomProperty =
             DependencyProperty.Register("Zoom",
             typeof(double),
             typeof(WindowPopupBehavior),
             new FrameworkPropertyMetadata(1.0));
        // .NET Property wrapper
        public double Zoom
        {
            get { return (double)GetValue(ZoomProperty); }
            set
            {
                SetValue(ZoomProperty, value);
            }
        }
        #endregion
    }
}
