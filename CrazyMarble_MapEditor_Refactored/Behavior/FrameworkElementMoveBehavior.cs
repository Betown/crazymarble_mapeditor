﻿using CrazyMarble_MapEditor.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Threading;

namespace CrazyMarble_MapEditor.Behavior
{
    // Datacontext MUST implement IMoveable.
    class FrameworkElementMoveBehavior : Behavior<FrameworkElement>
    {
        private static FrameworkElementMoveBehavior movingElement;

        private bool _isMouseDown;
        private bool _isKeyDown;
        private bool _isDragging;
        // Prevent Mouse.Capture from calling move event.
        private bool _isMoveEventDisabled = false;

        private Point _startPoint;
        private Point _originalPoint;
        private double _leftOffset = 0;
        private double _topOffset = 0;
        private FrameworkElementDragAdorner _overlayElement;

        private Size? _realSize;

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.MouseLeftButtonDown += AssociatedObject_MouseLeftButtonDown;
            AssociatedObject.MouseMove += AssociatedObject_MouseMove;
            AssociatedObject.MouseLeftButtonUp += AssociatedObject_MouseLeftButtonUp;
            AssociatedObject.PreviewLostKeyboardFocus += AssociatedObject_LostKeyboardFocus;
            AssociatedObject.PreviewKeyDown += AssociatedObject_PreviewKeyDown;
            AssociatedObject.PreviewKeyUp += AssociatedObject_PreviewKeyUp;

            AssociatedObject.LostFocus += AssociatedObject_LostFocus;
            if (SelfMoveable)
            {
                AssociatedObject.Drop += AssociatedObject_Drop;
            }

        }

        #region Mouse Controls

        void AssociatedObject_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (_isMouseDown || _isKeyDown || movingElement != null) return;
            movingElement = this;
            _isMouseDown = true;
            _startPoint = e.GetPosition(Container);
            _isMoveEventDisabled = true;
            AssociatedObject.CaptureMouse();
            _isMoveEventDisabled = false;
            e.Handled = true;
        }

        void AssociatedObject_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isMoveEventDisabled) return;
            if (!_isMouseDown || _isKeyDown) return;
            // Drag start
            if ((!_isDragging) &&
                ((Math.Abs(e.GetPosition(Container).X - _startPoint.X) > SystemParameters.MinimumHorizontalDragDistance) ||
                (Math.Abs(e.GetPosition(Container).Y - _startPoint.Y) > SystemParameters.MinimumVerticalDragDistance)))
            {
                _isDragging = true;
                _originalPoint = AssociatedObject.TranslatePoint(new Point(0.0, 0.0), Container);
                AssociatedObject.Opacity = .5;

                _overlayElement = new FrameworkElementDragAdorner(AssociatedObject, _originalPoint, Zoom);
                AdornerLayer layer = AdornerLayer.GetAdornerLayer(AssociatedObject);
                layer.Add(_overlayElement);

                _startPoint = new Point(_originalPoint.X + AssociatedObject.ActualWidth / 2,
                    _originalPoint.Y + AssociatedObject.ActualHeight / 2);

                IMoveable dragObject = this.AssociatedObject.DataContext as IMoveable;
                dragObject.OnMoveStart();
            }
            // Dragging...
            if (_isDragging)
            {
                Point currentPosition = System.Windows.Input.Mouse.GetPosition(Container);
                currentPosition = adjustCoordinatesContainer(currentPosition);
                currentPosition = adjustCoordinatesGrid(currentPosition);

                _overlayElement.LeftOffset = currentPosition.X - _startPoint.X;
                _overlayElement.TopOffset = currentPosition.Y - _startPoint.Y;

                _leftOffset = currentPosition.X - _startPoint.X;
                _topOffset = currentPosition.Y - _startPoint.Y;
            }
        }
        
        void AssociatedObject_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_isMouseDown && !_isKeyDown)
            {
                _isMoveEventDisabled = true;
                System.Windows.Input.Mouse.Capture(null);
                _isMoveEventDisabled = false;
                if (_isDragging)
                {
                    stopMove();
                    e.Handled = true;
                }
                AssociatedObject.Focus();
                _isMouseDown = false;
                movingElement = null;
            }
        }

        // WARNING!
        // Works properly only if Container is set and associatedobject does not have MovePanelbehavior.
        void AssociatedObject_Drop(object sender, DragEventArgs e)
        {
            // if the data type can be dropped 
            if (e.Data.GetDataPresent(typeof(IMoveable)) &&
                e.Data.GetDataPresent(typeof(Vector)))
            {
                // Start movement
                IMoveable source = e.Data.GetData(typeof(IMoveable)) as IMoveable;
                source.Move(source, (Vector)e.Data.GetData(typeof(Vector)));
                // Finalize movement
                IMovePanel target = Container.DataContext as IMovePanel;
                target.MoveEnd(source, (Vector)e.Data.GetData(typeof(Vector)));
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        #endregion

        #region Keyboard Control

        void AssociatedObject_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (_isMouseDown) return;

            if ((e.Key == Key.Up || e.Key == Key.Down || e.Key == Key.Left || e.Key == Key.Right) ||
                (_isKeyDown && (Keyboard.Modifiers.HasFlag(ModifierKeys.Control) || Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))))
            {
                int dX = 0, dY = 0;
                if (Keyboard.IsKeyDown(Key.Up)) dY--;
                if (Keyboard.IsKeyDown(Key.Down)) dY++;
                if (Keyboard.IsKeyDown(Key.Left)) dX--;
                if (Keyboard.IsKeyDown(Key.Right)) dX++;

                getKeyboardMoveDistanceMultiplier(ref dX, ref dY);

                // Drag start
                if (!_isDragging || movingElement == null)
                {
                    movingElement = this;
                    _leftOffset = 0;
                    _topOffset = 0;

                    _isDragging = true;
                    _originalPoint = AssociatedObject.TranslatePoint(new Point(0.0, 0.0), Container);
                    AssociatedObject.Opacity = .5;

                    _overlayElement = new FrameworkElementDragAdorner(AssociatedObject, _originalPoint, Zoom);
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(AssociatedObject);
                    layer.Add(_overlayElement);

                    _startPoint = new Point(_originalPoint.X + AssociatedObject.ActualWidth / 2,
                        _originalPoint.Y + AssociatedObject.ActualHeight / 2);

                    IMoveable dragObject = this.AssociatedObject.DataContext as IMoveable;
                    dragObject.OnMoveStart();
                }
                _leftOffset += dX;
                _topOffset += dY;
                Point currentPosition = new Point(_startPoint.X + _leftOffset, _startPoint.Y + _topOffset);
                currentPosition = adjustCoordinatesContainer(currentPosition);
                if (Math.Abs(dX) > 1 || Math.Abs(dY) > 1) currentPosition = adjustCoordinatesGrid(currentPosition);

                _overlayElement.LeftOffset = currentPosition.X - _startPoint.X;
                _overlayElement.TopOffset = currentPosition.Y - _startPoint.Y;

                _leftOffset = currentPosition.X - _startPoint.X;
                _topOffset = currentPosition.Y - _startPoint.Y;

                e.Handled = true;
                _isKeyDown = true;
            }
        }

        private void getKeyboardMoveDistanceMultiplier(ref int dX, ref int dY)
        {
            if (Grid != null && Grid.IsEnabled)
            {
                if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
                {
                    // Both Ctrl + Shift at the same time
                    if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                    {
                        dX *= (int)(2 * Grid.Size.Width);
                        dY *= (int)(2 * Grid.Size.Height);
                    }
                    else
                    {
                        dX *= (int)(0.5 * Grid.Size.Width);
                        dY *= (int)(0.5 * Grid.Size.Height);
                    }

                }
                else if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                {
                    dX *= (int)Grid.Size.Width;
                    dY *= (int)Grid.Size.Height;
                }
            }
            else
            {
                if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
                {
                    // Both Ctrl + Shift at the same time
                    if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                    {
                        dX *= 48;
                        dY *= 48;
                    }
                    else
                    {
                        dX *= 5;
                        dY *= 5;
                    }

                }
                else if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                {
                    dX *= 16;
                    dY *= 16;
                }
            }
        }

        private void AssociatedObject_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (_isKeyDown && !Keyboard.Modifiers.HasFlag(ModifierKeys.Control) && 
                !Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
            {
                if (_isDragging && Keyboard.IsKeyUp(Key.Up) && Keyboard.IsKeyUp(Key.Down) 
                    && Keyboard.IsKeyUp(Key.Left) && Keyboard.IsKeyUp(Key.Right))
                {
                    stopMove();
                    e.Handled = true;
                }
            }
        }

        #endregion

        #region move adjustments

        private Point adjustCoordinatesGrid(Point currentPosition)
        {
            if (Grid == null || !Grid.IsEnabled) return currentPosition;
            var snapX = currentPosition.X;
            var snapY = currentPosition.Y;
            var gapX = snapX % Grid.Size.Width;
            var gapY = snapY % Grid.Size.Height;
            if (gapX < Grid.Size.Width * GridSnappingSensitivity + 1.0)
            {
                snapX = snapX - snapX % Grid.Size.Width;
            }
            else if (gapX > Grid.Size.Width * (1.0 - GridSnappingSensitivity) - 1.0)
            {
                snapX = ((int)(snapX / Grid.Size.Width) + 1.0) * Grid.Size.Width;
            }

            if (gapY < Grid.Size.Height * GridSnappingSensitivity + 1.0)
            {
                snapY = snapY - snapY % Grid.Size.Height;
            }
            else if (gapY > Grid.Size.Height * (1.0 - GridSnappingSensitivity) - 1.0)
            {
                snapY = ((int)(snapY / Grid.Size.Height) + 1.0) * Grid.Size.Height;
            }

            return new Point(snapX, snapY);
        }

        private Point adjustCoordinatesContainer(Point currentPosition)
        {
            if (Container == null) return currentPosition;

            double halfW, halfH;

            if (UseRealSize)
            {
                var size = (Size)AssociatedObject.GetValue(AttachedProperties.EffectiveSizeProperty);
                if (size.Width > 0 && size.Height > 0)
                {
                    _realSize = size;
                }
                else
                {
                    size = (Size)AssociatedObject.GetValue(AttachedProperties.RealSizeProperty);
                    if (size.Width > 0 && size.Height > 0)
                    {
                        _realSize = size;
                    }
                }
                if (_realSize != null)
                {
                    halfW = _realSize.Value.Width / 2;
                    halfH = _realSize.Value.Height / 2;
                }
                else
                {
                    halfW = AssociatedObject.ActualWidth / 2;
                    halfH = AssociatedObject.ActualHeight / 2;
                }
            }
            else
            {
                halfW = AssociatedObject.ActualWidth / 2;
                halfH = AssociatedObject.ActualHeight / 2;
            }

            var snapX = Math.Max(currentPosition.X, halfW);
            var snapY = Math.Max(currentPosition.Y, halfH);
            snapX = Math.Min(snapX, Container.ActualWidth - halfW);
            snapY = Math.Min(snapY, Container.ActualHeight - halfH);

            return new Point(snapX, snapY);
        }

        // Prevent lost focusing while dragging
        private void AssociatedObject_LostFocus(object sender, RoutedEventArgs e)
        {
            if (_isDragging)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(() => AssociatedObject.Focus()));
            }
        }
        private void AssociatedObject_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (_isDragging)
            {
                StopMove();
                e.Handled = true;
            }
        }
        public static void StopMove()
        {
            if(movingElement != null)
                movingElement.stopMove();
        }

        private void stopMove()
        {
            if (!_isDragging || movingElement == null) return;

            AdornerLayer.GetAdornerLayer(AssociatedObject).Remove(_overlayElement);
            _overlayElement = null;

            AssociatedObject.Opacity = 1;

            //set the item's DataContext as the data to be transferred
            IMoveable dragObject = this.AssociatedObject.DataContext as IMoveable;
            if (dragObject != null)
            {
                // Start movement
                IMoveable source = AssociatedObject.DataContext as IMoveable;
                source.Move(source, new Vector(_leftOffset, _topOffset));
                // Finalize movement
                IMovePanel target = Container.DataContext as IMovePanel;
                target.MoveEnd(source, new Vector(_leftOffset, _topOffset));
            }

            movingElement = null;

            _isDragging = false;
            _isKeyDown = false;
            _isMouseDown = false;
        }

        #endregion


        #region Dependency Property
        // Dependency Property
        public static readonly DependencyProperty ContainerProperty =
             DependencyProperty.Register("Container", 
             typeof(FrameworkElement),
             typeof(FrameworkElementMoveBehavior), 
             new FrameworkPropertyMetadata(null, 
                 FrameworkPropertyMetadataOptions.AffectsArrange));

        // .NET Property wrapper
        public FrameworkElement Container
        {
            get { return (FrameworkElement)GetValue(ContainerProperty); }
            set { SetValue(ContainerProperty, value); }
        }

        // Dependency Property
        public static readonly DependencyProperty GridProperty =
            DependencyProperty.Register("Grid",
            typeof(ViewModel.GridViewModel),
            typeof(FrameworkElementMoveBehavior),
            new PropertyMetadata(null));
        // .NET Property wrapper
        public ViewModel.GridViewModel Grid
        {
            get { return (ViewModel.GridViewModel)GetValue(GridProperty); }
            set { SetValue(GridProperty, value); }
        }

        // Dependency Property
        public static readonly DependencyProperty ZoomProperty =
            DependencyProperty.Register("Zoom",
            typeof(double),
            typeof(FrameworkElementMoveBehavior),
            new PropertyMetadata(1.0, new PropertyChangedCallback(OnZoomChanged)));
        // .NET Property wrapper
        public double Zoom
        {
            get { return (double)GetValue(ZoomProperty); }
            set { SetValue(ZoomProperty, value); }
        }
        private static void OnZoomChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = d as FrameworkElementMoveBehavior;
            if (obj._overlayElement != null)
            {
                obj._overlayElement.Zoom = (double)e.NewValue;
            }
        }

        // Dependency Property
        public static readonly DependencyProperty GridSnappingSensitivityProperty =
            DependencyProperty.Register("GridSnappingSensitivity", 
            typeof(double), 
            typeof(FrameworkElementMoveBehavior), 
            new PropertyMetadata(0.2));

        // .NET Property wrapper
        public double GridSnappingSensitivity
        {
            get { return (double)GetValue(GridSnappingSensitivityProperty); }
            set { SetValue(GridSnappingSensitivityProperty, value); }
        }


        // Dependency Property
        public static readonly DependencyProperty SelfMoveableProperty =
             DependencyProperty.Register("SelfMoveable",
             typeof(bool),
             typeof(FrameworkElementMoveBehavior),
             new FrameworkPropertyMetadata(false, 
                 FrameworkPropertyMetadataOptions.AffectsArrange));

        // .NET Property wrapper
        public bool SelfMoveable
        {
            get { return (bool)GetValue(SelfMoveableProperty); }
            set { SetValue(SelfMoveableProperty, value); }
        }

        // Dependency Property
        public static readonly DependencyProperty UseRealSizeProperty =
             DependencyProperty.Register("UseRealSize",
             typeof(bool),
             typeof(FrameworkElementMoveBehavior),
             new FrameworkPropertyMetadata(true,
                 FrameworkPropertyMetadataOptions.AffectsArrange));
        // .NET Property wrapper
        public bool UseRealSize
        {
            get { return (bool)GetValue(UseRealSizeProperty); }
            set { SetCurrentValue(UseRealSizeProperty, value); }
        }
        #endregion
    }
}
