﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CrazyMarble_MapEditor.Behavior
{
    public interface IConnectable
    {
        double X { get; }
        double Y { get; }

        double Width { get; }
        double Height { get; }

        Type ConnectableType { get; }
        Type ConnectableConnectorType { get; }

        void RegisterConnector(IConnector connector);
        void UnregisterConnector(IConnector connector);
        bool IsConnectValid(IConnector connector, IConnectable target);
        void OnConnect(IConnector connector, IConnectable target);
        void OnDisconnect(IConnector connector, IConnectable target);
    }

    public interface IConnector
    {
        IConnectable Start { get; }
        IConnectable End { get; }

        Type ConnectorType { get; }

        void Connect(IConnectable target);
        void OnConnectFailed(Vector disp);

        void Update();
        void Disconnect(IConnectable target);
    }
}
