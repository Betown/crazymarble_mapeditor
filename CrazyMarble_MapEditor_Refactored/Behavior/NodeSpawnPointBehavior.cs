﻿using CrazyMarble_MapEditor.Util;
using CrazyMarble_MapEditor.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Threading;

namespace CrazyMarble_MapEditor.Behavior
{
    class NodeSpawnPointBehavior : Behavior<FrameworkElement>
    {
        private bool _isPointed;
        private Point _startPoint;

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.MouseLeftButtonDown += AssociatedObject_MouseLeftButtonDown;
            this.AssociatedObject.MouseMove += AssociatedObject_MouseMove;
            this.AssociatedObject.MouseLeftButtonUp += AssociatedObject_MouseLeftButtonUp;
        }

        private void AssociatedObject_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Prevent event during special key press
            if (UtilFunc.IsAnySpecialKeyDown()) return;

            AssociatedObject.Focus();
            _startPoint = e.GetPosition(AssociatedObject);
            _isPointed = true;
        }

        private void AssociatedObject_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isPointed &&
                ((Math.Abs(e.GetPosition(AssociatedObject).X - _startPoint.X) > SystemParameters.MinimumHorizontalDragDistance) ||
                 (Math.Abs(e.GetPosition(AssociatedObject).Y - _startPoint.Y) > SystemParameters.MinimumVerticalDragDistance)))
            {
                _isPointed = false;
            }
        }

        private void AssociatedObject_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_isPointed)
            {
                // Create NodeSpawnPoint
                var map = AssociatedObject.DataContext as MapViewModel;
                if (map != null)
                {
                    var point = _startPoint;

                    if (SpawnElementSize != null)
                    {
                        var size = SpawnElementSize.Value;
                        var snapX = Math.Max(_startPoint.X, size.Width / 2);
                        var snapY = Math.Max(_startPoint.Y, size.Height / 2);
                        snapX = Math.Min(snapX, AssociatedObject.ActualWidth - size.Width / 2);
                        snapY = Math.Min(snapY, AssociatedObject.ActualHeight - size.Height / 2);
                        _startPoint = new Point(snapX, snapY);
                    }

                    map.NodeSpawnPoint = _startPoint;
                }
                _isPointed = false;
            }
        }

        #region Dependency Property
        // Dependency Property
        public static readonly DependencyProperty SpawnElementSizeProperty =
             DependencyProperty.Register("SpawnElementSize",
             typeof(Size?),
             typeof(NodeSpawnPointBehavior),
             new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange));

        // .NET Property wrapper
        public Size? SpawnElementSize
        {
            get { return (Size?)GetValue(SpawnElementSizeProperty); }
            set { SetValue(SpawnElementSizeProperty, value); }
        }
        #endregion
    }
}
