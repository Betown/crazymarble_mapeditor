﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace CrazyMarble_MapEditor.Behavior
{
    class FrameworkElementMovePanelBehavior : Behavior<FrameworkElement>
    {
        private Type dataType; //the type of the data that can be dropped into this control

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.DragEnter += AssociatedObject_DragEnter;
            AssociatedObject.DragOver += AssociatedObject_DragOver;
            AssociatedObject.DragLeave += AssociatedObject_DragLeave;
            AssociatedObject.Drop += AssociatedObject_Drop;
        }

        void AssociatedObject_Drop(object sender, DragEventArgs e)
        {
            if (dataType != null)
            {
                // if the data type can be dropped 
                if (e.Data.GetDataPresent(typeof(IMoveable)) && 
                    e.Data.GetDataPresent(typeof(Vector)))
                {
                    // Start movement
                    IMoveable source = e.Data.GetData(typeof(IMoveable)) as IMoveable;
                    if (dataType.IsInstanceOfType(source))
                    {
                        source.Move(source, (Vector)e.Data.GetData(typeof(Vector)));
                        // Finalize movement
                        IMovePanel target = this.AssociatedObject.DataContext as IMovePanel;
                        target.MoveEnd(source, (Vector)e.Data.GetData(typeof(Vector)));
                    }
                    else
                    {
                        e.Effects = DragDropEffects.None;
                    }
                }
                else
                {
                    e.Effects = DragDropEffects.None;
                }

                // Check if connector
                if (e.Data.GetDataPresent(typeof(IConnector)) &&
                    e.Data.GetDataPresent(typeof(Vector)))
                {
                    IConnector source = e.Data.GetData(typeof(IConnector)) as IConnector;
                    if (!typeof(IConnectable).IsInstanceOfType(this.AssociatedObject.DataContext))
                    {
                        (source as IConnector).OnConnectFailed((Vector)e.Data.GetData(typeof(Vector)));
                        e.Effects = DragDropEffects.None;
                    }
                }
            }

            e.Handled = true;
            return;
        }

        void AssociatedObject_DragLeave(object sender, DragEventArgs e)
        {
            e.Handled = true;
        }

        void AssociatedObject_DragOver(object sender, DragEventArgs e)
        {
            if (dataType != null)
            {
                //if item can be dropped
                if (e.Data.GetDataPresent(typeof(IMoveable)) &&
                    dataType.IsInstanceOfType(e.Data.GetData(typeof(IMoveable))))
                {
                    //give mouse effect
                    e.Effects = DragDropEffects.Copy;
                }
                else
                {
                    e.Effects = DragDropEffects.None;
                }
            }
            e.Handled = true;
        }

        void AssociatedObject_DragEnter(object sender, DragEventArgs e)
        {
            //if the DataContext implements IDropable, record the data type that can be dropped
            if (this.dataType == null)
            {
                if (this.AssociatedObject.DataContext != null)
                {
                    IMovePanel dropObject = this.AssociatedObject.DataContext as IMovePanel;
                    if (dropObject != null)
                    {
                        this.dataType = dropObject.DataType;
                    }
                }
            }
            e.Handled = true;
        }
    }
}
