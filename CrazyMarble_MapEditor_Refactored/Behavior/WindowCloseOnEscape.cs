﻿using CrazyMarble_MapEditor.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Threading;

namespace CrazyMarble_MapEditor.Behavior
{
    class WindowCloseOnEscapeBehavior : Behavior<Window>
    {

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.PreviewKeyUp += AssociatedObject_PreviewKeyUp;
        }

        void AssociatedObject_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                (AssociatedObject as Window).Close();
            }
        }
    }
}
