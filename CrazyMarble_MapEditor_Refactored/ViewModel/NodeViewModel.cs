﻿using CrazyMarble_MapEditor.Behavior;
using CrazyMarble_MapEditor.Model;
using CrazyMarble_MapEditor.Properties;
using CrazyMarble_MapEditor.Util;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;

namespace CrazyMarble_MapEditor.ViewModel
{
    [JsonConverter(typeof(NodeViewModel.NodeViewModelConverter))]
    public class NodeViewModel : ViewModelBase, IMoveable, IConnectable
    {
        #region Constructor / Copy
        private NodeViewModel()
        {
            SelectNodeCommand = new RelayCommand(SelectNode);
            NextIfArrivedEdgeCommand = new RelayCommand(NextIfArrivedCreateDelete);
            JumpToEdgeCommand = new RelayCommand(JumpToCreateDelete);
        }

        public NodeViewModel(int _ID, double X = 0, double Y = 0)
            : this()
        {
            _node = new Node{
                ID = _ID, 
                InitialLevel = 1, 
                Type = Node.SpaceType.Property,
                Position = new Node.Point((int)Math.Round(X), (int)Math.Round(Y))
            };
        }

        // For deserialization
        private NodeViewModel(Node node)
            : this()
        {
            _node = new Node(node);
        }
        // Happens at Paste
        public NodeViewModel(NodeViewModel source, int newID, double X = 0, double Y = 0)
            : this()
        {
            _node = new Node(source.Node);
            _node.Position = new Node.Point((int)Math.Round(X), (int)Math.Round(Y));
            _node.ID.Value = newID;
        }
        // Copy
        public NodeViewModel Copy()
        {
            var clone = new NodeViewModel();
            clone._node = new Node(this.Node);
            return clone;
        }
        #endregion

        #region Members
        private Node _node;
        private int _diameter = 100;

        private bool _isSelected;

        private bool _isNextIfArrivedExist;
        private bool _isJumpToExist;
        private HashSet<IConnector> _edges = new HashSet<IConnector>();
        private IConnector _edgeNextIfArrived;
        private IConnector _edgeJumpTo;
        #endregion

        #region Properties
        private Node Node { 
            get { return _node; } 
        }
        public Integer ReferenceID { 
            get { return _node.ID; } 
        }
        public int ID { 
            get { return _node.ID; }
            set {
                //CommandManager.ExecuteNewCommand<NodeViewModel>(
                //    new UndoablePropertyChangeCommand<NodeViewModel>("ID", this, _node.ID, value));
                _node.ID.Value = value; 
                RaisePropertyChanged("ID"); 
            } 
        }
        public int? Next
        {
            get { return _node.Next; }
            set {
                _node.Next = value;
                RaisePropertyChanged("Next");
            }
        }
        public int? NextIfArrived
        {
            get { return _node.NextIfArrived; }
            set
            {
                _node.NextIfArrived = value;
                RaisePropertyChanged("NextIfArrived");
            }
        }
        public int? JumpTo
        {
            get { return _node.JumpTo; }
            set
            {
                _node.JumpTo = value;
                RaisePropertyChanged("JumpTo");
            }
        }

        public string Type { 
            get { return _node.Type.ToString(); }
            set
            {
                if (Type != value)
                {
                    string[] properties = { "Type", "InitialLevel" };
                    object[] oldValues = { _node.Type.ToString(), InitialLevel };
                    object[] newValues = { value, _node.TryCoerceInitialLevel(value) };


                    CommandManager.ExecuteNewCommand<NodeViewModel>(
                        new UndoablePropertiesChangeCommand<NodeViewModel>(
                            properties, this, oldValues, newValues, Resources.NodePropType));
                }
                _node.Type = value;
                RaisePropertyChanged("Type");
                RaisePropertyChanged("TypeName");
                RaisePropertyChanged("MaxLevel");
                RaisePropertyChanged("InitialLevel");
                RaisePropertyChanged("IconPath");
            } 
        }
        public string TypeName
        {
            get { return Node.SpaceType.ToOriginalName(_node.Type); }
        }
        public int InitialLevel
        {
            get { return _node.InitialLevel; }
            set {
                if (value > 0 && value <= MaxLevel)
                {
                    if (_node.InitialLevel != value)
                    {
                        CommandManager.ExecuteNewCommand<NodeViewModel>(
                            new UndoablePropertyChangeCommand<NodeViewModel>(
                                "InitialLevel", this, _node.InitialLevel, value, Resources.NodePropInitialLevel));
                    }
                    _node.InitialLevel = value;
                    RaisePropertyChanged("InitialLevel");
                    RaisePropertyChanged("IconPath");
                }
            }
        }
        public int MaxLevel
        {
            get { return Node.MaxLevel; }
        }

        public int X
        {
            get { return _node.Position.X; }
            set { 
                _node.Position.X.Value = value; 
                RaisePropertyChanged("X");
            }
        }
        public int Y
        {
            get { return _node.Position.Y; }
            set { 
                _node.Position.Y.Value = value; 
                RaisePropertyChanged("Y");
            }
        }
        public double Width
        {
            get { return _diameter; }
        }
        public double Height
        {
            get { return _diameter; }
        }

        public static ICollection<string> SpaceTypes
        {
            get { return Node.SpaceTypes; }
        }
        public static ICollection<string> SpaceTypeNames
        {
            get { return Node.SpaceTypeNames; }
        }
        #endregion

        #region Properties(UI only)
        public int ZPriority { get { return 1; } }

        public string IconPath
        {
            get
            {
                string filePath = @"..\Image\Space\Space_" +
                    Node.SpaceType.ToOriginalName(_node.Type);
                var lvLimit = _node.InitialLevel;
                if (_node.Type == Node.SpaceType.Property)
                {
                    lvLimit = Math.Min(5, lvLimit);
                    filePath += "Lv" + lvLimit;
                }
                else if (_node.Type == Node.SpaceType.Chest)
                {
                    lvLimit = Math.Min(3, lvLimit);
                    filePath += "Lv" + lvLimit;
                }
                filePath += ".png";
                return filePath;
            }
        }

        public int Diameter
        {
            get { return _diameter; }
            set { 
                _diameter = value;
                RaisePropertyChanged("Diameter");
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set {
                _isSelected = value;
                RaisePropertyChanged("IsSelected");
            }
        }

        public bool IsNextIfArrivedExist
        {
            get { return _isNextIfArrivedExist; }
            private set { 
                _isNextIfArrivedExist = value;
                RaisePropertyChanged("IsNextIfArrivedExist");
            }
        }
        public bool IsJumpToExist
        {
            get { return _isJumpToExist; }
            private set
            {
                _isJumpToExist = value;
                RaisePropertyChanged("IsJumpToExist");
            }
        }
        #endregion

        #region Properties(Edges)

        public HashSet<IConnector> Edges
        {
            get { return _edges; }
        }

        #endregion

        #region IMoveable Members
        Type IMoveable.DataType
        {
            get{ return typeof(NodeViewModel); }
        }

        double IMoveable.X
        {
            get { return X; }
        }
        double IMoveable.Y
        {
            get { return Y; }
        }
        double IMoveable.Width
        {
            get { return Diameter; }
        }
        double IMoveable.Height
        {
            get { return Diameter; }
        }

        public void Move(object data, Vector disp)
        {
            MessengerInstance.Send<NodeChangedMessage>(
                new NodeChangedMessage { ID = this.ID, State = NodeChangedMessage.NodeState.Moved });
            CommandManager.ExecuteNewCommand(new MoveNodeCommand(this, disp));
        }
        
        public void OnMoveStart()
        {
            MessengerInstance.Send<NodeChangedMessage>(
                new NodeChangedMessage { ID = this.ID, State = NodeChangedMessage.NodeState.Moving });
        }
        #endregion

        #region IConnectable Members

        #region IConnectable Properties
        double IConnectable.X
        {
            get { return X; }
        }
        double IConnectable.Y
        {
            get { return Y; }
        }
        double IConnectable.Width
        {
            get { return _diameter; }
        }
        double IConnectable.Height
        {
            get { return _diameter; }
        }

        Type IConnectable.ConnectableType
        {
            get { return typeof(NodeViewModel); }
        }

        Type IConnectable.ConnectableConnectorType
        {
            get { return typeof(EdgeViewModel); }
        }

        #endregion

        public void RegisterConnector(IConnector connector)
        {
            _edges.Add(connector);
            if (connector.Start == this)
            {
                var edge = connector as EdgeViewModel;
                switch (edge.Type)
                {
                    case EdgeType.NextIfArrived:
                        _edgeNextIfArrived = edge;
                        IsNextIfArrivedExist = true;
                        break;
                    case EdgeType.JumpTo:
                        _edgeJumpTo = edge;
                        IsJumpToExist = true;
                        break;
                }
            }
        }
        public void UnregisterConnector(IConnector connector)
        {
            _edges.Remove(connector); 
            if (connector.Start == this)
            {
                var edge = connector as EdgeViewModel;
                switch (edge.Type)
                {
                    case EdgeType.NextIfArrived:
                        _edgeNextIfArrived = null;
                        IsNextIfArrivedExist = false;
                        break;
                    case EdgeType.JumpTo:
                        _edgeJumpTo = null;
                        IsJumpToExist = false;
                        break;
                }
            }
        }

        public bool IsConnectValid(IConnector connector, IConnectable target)
        {
            var node = target as NodeViewModel;
            return !node._node.PrevList.Contains(ReferenceID) &&
                target != this;
        }
        public void OnConnect(IConnector connector, IConnectable target)
        {
            //_edges.Add(connector);
            RegisterConnector(connector);
            var edge = connector as EdgeViewModel;
            var node = target as NodeViewModel;
            if (connector.Start == this)
            {
                _node.GetType().GetProperty(edge.Type.ToString()).SetValue(_node, node.ReferenceID);
            }
            else
            {
                _node.PrevList.Add(node.ReferenceID);
            }
        }
        public void OnDisconnect(IConnector connector, IConnectable target)
        {
            //_edges.Remove(connector);
            var edge = connector as EdgeViewModel;
            var node = target as NodeViewModel;
            if (connector.Start == this)
            {
                _node.GetType().GetProperty(edge.Type.ToString()).SetValue(_node, null);
            }
            else
            {
                _node.PrevList.Remove(node.ReferenceID);
                UnregisterConnector(connector);
            }
        }
        #endregion

        #region Commands Properties
        public RelayCommand SelectNodeCommand
        {
            get;
            private set;
        }

        public RelayCommand NextIfArrivedEdgeCommand
        {
            get;
            private set;
        }
        public RelayCommand JumpToEdgeCommand
        {
            get;
            private set;
        }
        #endregion

        #region Commands Implementations
        public void SelectNode()
        {
            MessengerInstance.Send<NodeSelectMessage>(new NodeSelectMessage { ID = this.ID });
        }

        // Since change of IsChecked(ToggleButton) is Executed prior to command,
        // the value of flag is supposed to be work opposite.
        public void NextIfArrivedCreateDelete()
        {
            // Did not exist; create new edge
            if (IsNextIfArrivedExist)
            {
                MessengerInstance.Send<EdgeCreateMessage>(
                    new EdgeCreateMessage { ID = this.ID, Type = EdgeType.NextIfArrived });
            }
            // Did exist; delete edge
            else
            {
                MessengerInstance.Send<EdgeDeleteMessage>(
                    new EdgeDeleteMessage { Edge = _edgeNextIfArrived });
            }
        }
        public void JumpToCreateDelete()
        {
            // Did not exist; create new edge
            if (IsJumpToExist)
            {
                MessengerInstance.Send<EdgeCreateMessage>(
                    new EdgeCreateMessage { ID = this.ID, Type = EdgeType.JumpTo });
            }
            // Did exist; delete edge
            else
            {
                MessengerInstance.Send<EdgeDeleteMessage>(
                    new EdgeDeleteMessage { Edge = _edgeJumpTo });
            }
        }
        #endregion

        #region Undoable Command Implementations

        #region Move Node
        public class MoveNodeCommand : UndoableCommand
        {
            private static IMessenger _messengerInstance;
            private NodeViewModel _node;
            private HashSet<IConnector> _edges;
            private int _X1, _X2, _Y1, _Y2;

            public MoveNodeCommand(NodeViewModel node, Vector disp)
            {

                if (_messengerInstance == null)
                {
                    _messengerInstance = node.MessengerInstance;
                }

                _node = node;
                _edges = node._edges;
                _X1 = node.X;
                _Y1 = node.Y;
                _X2 = _X1 + (int)Math.Round(disp.X);
                _Y2 = _Y1 + (int)Math.Round(disp.Y);
            }

            public override string Discription
            {
                get { return Resources.CommandMoveNode; }
            }

            public override void Execute()
            {
                _node.X = _X2;
                _node.Y = _Y2;
                foreach (IConnector edge in _edges)
                {
                    edge.Update();
                }
                _messengerInstance.Send<NodeChangedMessage>(
                    new NodeChangedMessage
                    {
                        ID = _node.ID,
                        State = NodeChangedMessage.NodeState.Moved
                    });
            }

            public override void UnExecute()
            {
                _node.X = _X1;
                _node.Y = _Y1;
                foreach (IConnector edge in _edges)
                {
                    edge.Update();
                }
                _messengerInstance.Send<NodeChangedMessage>(
                    new NodeChangedMessage { 
                        ID = _node.ID, State = NodeChangedMessage.NodeState.Moved});
            }
        }
        #endregion

        #endregion

        #region Json Converter
        public class NodeViewModelConverter : JsonConverter
        {
            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                var obj = JToken.FromObject((value as NodeViewModel)._node) as JObject;
                obj.WriteTo(writer);
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                var nodeData = serializer.Deserialize<Node>(reader);
                // Since prevlist has list of Integer values, 
                // clear irrevalent seperate objects in order to work properly afterwards
                // prevlist will be filled during reconstruction.
                nodeData.PrevList.Clear();  
                return new NodeViewModel(nodeData);
            }

            public override bool CanRead
            {
                get { return true; }
            }

            public override bool CanConvert(Type objectType)
            {
                return objectType == typeof(NodeViewModel);
            }
        }
        #endregion

        #region Object Members
        public override string ToString()
        {
            return string.Format(Resources.NodePropStringFormat, 
                ID, _node.Position, InitialLevel, Type);
        }
        #endregion
    }
}
