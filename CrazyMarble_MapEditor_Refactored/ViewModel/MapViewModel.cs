using CrazyMarble_MapEditor.Behavior;
using CrazyMarble_MapEditor.Properties;
using CrazyMarble_MapEditor.Util;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace CrazyMarble_MapEditor.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MapViewModel : ViewModelBase, IMovePanel
    {
        private const int MAX_NODECOUNT = 100;

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the MapViewModel class.
        /// </summary>
        public MapViewModel(IUIService UIservice, IRecentFileService recentFile)
        {
            // Set Interfaces
            _UIservice = UIservice;

            // Set Containers
            _data = new CompositeCollection();

            _nodes = new NodeCollection<NodeViewModel>();
            _nodes.ListChanged += _nodeListChanged;
            _edges = new BindingList<EdgeViewModel>();

            _data.Add(new CollectionContainer() { Collection = _nodes });
            _data.Add(new CollectionContainer() { Collection = _edges });

            _errorList = new BindingList<string>();
            _sizeSampleNode = new NodeViewModel(-1);

            // Initialize component viewmodels
            _statistics = new StatisticsViewModel(this);
            _gridManager = new GridViewModel();
            _zoomManager = new ZoomViewModel();

            // Initialize component classes
            _fileManager = new FileManager(this, UIservice, recentFile);

            // Set default values
            _setDefaultState();

            // Set commands
            _setRelayCommands();
            _setCommandBindings();

            // Set message listeners
            _setMessageListeners();

            // Dirty flag setting
            _nodes.ListChanged += _onPropertyChanged;
            _edges.ListChanged += _onPropertyChanged;
        }
        #endregion

        #region Members

        #region Data

        private string _mapName;

        private BindingList<NodeViewModel> _nodes;
        private BindingList<EdgeViewModel> _edges;
        private CompositeCollection _data;

        private string _status;

        #endregion

        #region Editing

        private CommandBindingCollection _commandBindings;

        private NodeViewModel _sizeSampleNode;
        private NodeViewModel _featuredNode;
        private NodeViewModel _copiedNode;

        private bool _isIgnoringAutoConnect;

        private bool _isNodePropertiesWindowOpen;
        private bool _isNodePropertiesWindowHidden;

        private bool _isDirty;

        private StatisticsViewModel _statistics;

        private NodeSpawnPointViewModel _nodeSpawnPoint;
        private GridViewModel _gridManager;
        private ZoomViewModel _zoomManager;

        #endregion

        #region File

        private FileManager _fileManager;

        private string _lastFilePath;

        private string _backgroundPath;
        private BitmapSource _background;
        private Size _backgroundSize;
        private string _backgroundSizeString;

        #endregion

        #region Verification / Modal Dialog

        private BindingList<string> _errorList;
        private IUIService _UIservice;

        #endregion

        #endregion

        #region Member Functions

        #region Initializations

        private void _setCommandBindings()
        {
            _commandBindings = new CommandBindingCollection();
            var commands = new List<CommandBinding>();

            // File Commands
            commands.Add(new CommandBinding(ApplicationCommands.New, NewFile, NewFileCanExecute));
            commands.Add(new CommandBinding(ApplicationCommands.Open, OpenFile));
            commands.Add(new CommandBinding(ApplicationCommands.Save, SaveFile));
            commands.Add(new CommandBinding(ApplicationCommands.SaveAs, SaveAs));
            commands.Add(new CommandBinding(Command.SetBackground, SetBackground));

            // Edit Commands
            commands.Add(new CommandBinding(Command.CreateNode, NewNode, NewNodeCanExecute));
            commands.Add(new CommandBinding(ApplicationCommands.Cut, MoveNode, MoveNodeCanExecute));
            commands.Add(new CommandBinding(ApplicationCommands.Copy, CopyNode, CopyNodeCanExecute));
            commands.Add(new CommandBinding(ApplicationCommands.Paste, PasteNode, PasteNodeCanExecute));
            commands.Add(new CommandBinding(ApplicationCommands.Delete, DeleteNode, DeleteNodeCanExecute));
            commands.Add(new CommandBinding(Command.Deselect, Deselect));
            commands.Add(new CommandBinding(Command.RemoveNodeSpawnPoint, RemoveNodeSpawnPoint));

            // Undo / Redo
            commands.Add(new CommandBinding(ApplicationCommands.Undo, Undo, UndoCanExecute));
            commands.Add(new CommandBinding(ApplicationCommands.Redo, Redo, RedoCanExecute));

            // Open Subwindows
            commands.Add(new CommandBinding(Command.Statistics, OpenStatistics));
            commands.Add(new CommandBinding(Command.BeforeSave, OpenBeforeSave));
            commands.Add(new CommandBinding(Command.ProgramInfo, OpenAbout));

            commands.ForEach(cmd =>
            System.Windows.Input.CommandManager.RegisterClassCommandBinding(typeof(MapViewModel), cmd));
            _commandBindings.AddRange(commands);
        }

        private void _setRelayCommands()
        {
            CloseNodePropertiesWindowCommand = new RelayCommand(ClosePropertiesWindow);

            BeforeCloseCommand = new RelayCommand<CancelEventArgs>(BeforeClose);

            NextNodeCommand = new RelayCommand(NextNode);
            PrevNodeCommand = new RelayCommand(PrevNode);
            ToggleGridCommand = new RelayCommand(ToggleGrid);

            OpenRecentFileCommand = new RelayCommand<string>(OpenRecentFile);

            RefreshErrorCommand = new RelayCommand(RefreshErrorList);

            SetEmptyBackgroundCommand = new RelayCommand<string>(SetEmptyBackground);
        }

        private void _setMessageListeners()
        {
            MessengerInstance.Register<EdgeCreateMessage>
                (this, msg => CreateEdge(msg.ID, msg.Type));
            MessengerInstance.Register<EdgeDeleteMessage>
                (this, msg => DeleteEdge(msg.Edge));
            MessengerInstance.Register<NodeChangedMessage>
                (this, msg => _updateNodePropertiesWindow(msg.ID, msg.State));
            MessengerInstance.Register<NodeSelectMessage>
                (this, msg => SelectNode(msg.ID));

            MessengerInstance.Register<NodeSpawnPointMessage>
                (this, msg => OnNodeSpawnPointMessage(msg));
        }

        #endregion

        #region Update/Reset Map States

        private void _updateNodePropertiesWindow(int ID, NodeChangedMessage.NodeState state)
        {
            if (Nodes[ID] == FeaturedNode)
            {
                switch (state)
                {
                    case NodeChangedMessage.NodeState.Moving:
                        if (IsNodePropertiesWindowOpen)
                        {
                            _isNodePropertiesWindowHidden = true;
                            IsNodePropertiesWindowOpen = false;
                        }
                        break;
                    case NodeChangedMessage.NodeState.Moved:
                        if (_isNodePropertiesWindowHidden)
                        {
                            IsNodePropertiesWindowOpen = true;
                            _isNodePropertiesWindowHidden = false;
                        }
                        break;
                    case NodeChangedMessage.NodeState.Deleted:
                        IsNodePropertiesWindowOpen = false;
                        FeaturedNode = null;
                        break;
                    default:
                        break;
                }
            }
        }

        public void UpdateNodePropertiesWindow()
        {
            if (IsNodePropertiesWindowOpen)
            {
                IsNodePropertiesWindowOpen = false;
                IsNodePropertiesWindowOpen = true;
            }
        }

        public void clearMapState()
        {
            IsNodePropertiesWindowOpen = false;
            FeaturedNode = null;
            Nodes.Clear();
            Edges.Clear();
            CrazyMarble_MapEditor.Util.CommandManager.ClearUndoables();
            Background = null;
            BackgroundPath = null;
            ErrorList.Clear();
            _setDefaultState();
        }

        private void _setDefaultState()
        {
            MapName = Resources.PropDefaultName;
            BackgroundPath = null;
            IsNodePropertiesWindowOpen = false;
            Status = Resources.StatusReady;
            NodeSpawnPoint = null;
        }

        private void _onPropertyChanged(object sender, ListChangedEventArgs e)
        {
            // Do not turn on dirty flag for IsSelected
            if (e.PropertyDescriptor != null && 
                e.PropertyDescriptor.Name == "IsSelected")
            {
                return;
            }

            IsDirty = true;
            RaisePropertyChanged("LastDone");
            RaisePropertyChanged("LastUndone");
            Statistics.UpdateCommand.Execute(null);
        }

        private void _updateStatus()
        {
            var statusList = new List<string>();
            if (FeaturedNode != null)
            {
                if (IsNodePropertiesWindowOpen)
                {
                    statusList.Add(string.Format(Resources.StatusNodeEditing + " ", FeaturedNode.ID));
                }
                else
                {
                    statusList.Add(string.Format(Resources.StatusNodeSelected + " ", FeaturedNode));
                }
            }
            if (NodeSpawnPoint != null)
            {
                statusList.Add(string.Format(Resources.StatusHasTarget + " ", NodeSpawnPoint));
            }

            string status = string.Empty;
            if (statusList.Count == 0)
            {
                status = Resources.StatusReady;
            }
            else
            {
                status = String.Join(" / ", statusList.ToArray());
            }

            Status = status;
        }

        #endregion

        #region Map Verification

        private void _nodeListChanged(object sender, ListChangedEventArgs e)
        {
            RaisePropertyChanged("Nodes");
        }

        public bool verify()
        {
            return MapVerifier.verify(this);
        }

        #endregion

        #endregion

        #region Properties

        #region Data

        public CompositeCollection Data
        {
            get { return _data; }
        }
        public BindingList<NodeViewModel> Nodes
        {
            get { return _nodes; }
        }
        public BindingList<EdgeViewModel> Edges
        {
            get { return _edges; }
        }
        public string MapName
        {
            get { return _mapName; }
            set { 
                _mapName = value;
                RaisePropertyChanged("MapName");
            }
        }
        public string LastFilePath
        {
            get { return _lastFilePath; }
            set {
                _lastFilePath = value;
            }
        }
        public bool IsDirty
        {
            get { return _isDirty; }
            set { _isDirty = value; }
        }
        public string Status
        {
            get { return _status; }
            set { 
                _status = value;
                RaisePropertyChanged("Status");
            }
        }

        #endregion

        #region Data Verification

        public BindingList<string> ErrorList
        {
            get {
                return _errorList;
            }
        }

        public int NodeLimit
        {
            get { return MAX_NODECOUNT; }
        }

        public Size MaxBackgroundSize
        {
            get { return _fileManager.MaxBackgroundSize; }
        }
        public Size MinBackgroundSize
        {
            get { return _fileManager.MinBackgroundSize; }
        }

        #endregion

        #region Background-Related
        public string BackgroundPath
        {
            get { return _backgroundPath; }
            set
            {
                _backgroundPath = value;
                RaisePropertyChanged("BackgroundPath");
            }
        }

        public BitmapSource Background
        {
            get { return _background; }
            set
            {
                _background = value;
                if (Background == null)
                {
                    BackgroundSize = new Size();
                    BackgroundSizeString = Resources.PropNoBackground;
                }
                else
                {
                    BackgroundSize = new Size(value.PixelWidth, value.PixelHeight);
                    BackgroundSizeString = string.Format(
                        Resources.PropBackgroundSize, value.PixelWidth, value.PixelHeight);
                }
                RaisePropertyChanged("Background");
            }
        }

        public Size BackgroundSize
        {
            get { return _backgroundSize; }
            set
            {
                _backgroundSize = value;
                RaisePropertyChanged("BackgroundSize");
            }
        }

        public string BackgroundSizeString
        {
            get { return _backgroundSizeString; }
            set
            {
                _backgroundSizeString = value;
                RaisePropertyChanged("BackgroundSizeString");
            }
        }

        #endregion

        #region Statistics

        public StatisticsViewModel Statistics
        {
            get { return _statistics; }
            set { _statistics = value; }
        }

        #endregion

        #region Editing
        public NodeViewModel FeaturedNode
        {
            get { return _featuredNode; }
            set
            {
                if (_featuredNode != null)
                {
                    _featuredNode.IsSelected = false;
                }
                if (value != null)
                {
                    value.IsSelected = true;
                }
                _featuredNode = value;
                _updateStatus();
                RaisePropertyChanged("FeaturedNode");
            }
        }

        public bool IsNodePropertiesWindowOpen
        {
            get { return _isNodePropertiesWindowOpen; }
            set
            {
                _isNodePropertiesWindowOpen = value;
                _updateStatus();
                RaisePropertyChanged("IsNodePropertiesWindowOpen");
            }
        }

        public string LastDone
        {
            get
            {
                return Util.CommandManager.LastDoneCommand();
            }
        }
        public Object LastDones
        {
            get
            {
                return Util.CommandManager.CommandListManager.DoneCommands;
            }
        }
        public string LastUndone
        {
            get
            {
                return Util.CommandManager.LastUndoneCommand();
            }
        }
        public Object LastUndones
        {
            get
            {
                return Util.CommandManager.CommandListManager.UndoneCommands;
            }
        }

        public bool IsIgnoringAutoConnect
        {
            get { return _isIgnoringAutoConnect; }
            set { 
                _isIgnoringAutoConnect = value;
                RaisePropertyChanged("IsIgnoringAutoConnect");
            }
        }

        public Point? NodeSpawnPoint
        {
            get 
            {
                if (_nodeSpawnPoint == null) return null;
                else return _nodeSpawnPoint.SpawnPoint;
            }
            set 
            {
                if (_nodeSpawnPoint != null)
                {
                    _data.Remove(_nodeSpawnPoint);
                }

                if (value.HasValue)
                {
                    _nodeSpawnPoint =
                        new NodeSpawnPointViewModel(_sizeSampleNode, value.Value.X, value.Value.Y);
                    _data.Add(_nodeSpawnPoint);
                }
                else
                {
                    _nodeSpawnPoint = null;
                }

                _updateStatus();
                RaisePropertyChanged("NodeSpawnPoint");
                RaisePropertyChanged("SpawnElementSize");
            }
        }

        public Size? SpawnElementSize
        {
            get
            {
                if (_nodeSpawnPoint == null)
                {
                    var node = _sizeSampleNode as IMoveable;
                    return new Size(node.Width, node.Height);
                }
                else
                {
                    return _nodeSpawnPoint.SpawnElementSize;
                }
            }
        }

        public GridViewModel Grid
        {
            get { return _gridManager; }
        }

        public ZoomViewModel Zoom
        {
            get { return _zoomManager; }
        }
        #endregion

        #endregion

        #region IMovePanel members
        Type IMovePanel.DataType
        {
            get { return typeof(IMoveable); }
        }

        /// <summary>
        /// Movement finishes
        /// </summary>
        void IMovePanel.MoveEnd(object data, Vector disp)
        {
            
        }
        #endregion

        #region Commands

        public System.Windows.Input.CommandBindingCollection CommandBindings
        {
            get
            {
                return _commandBindings;
            }
        }
        public RelayCommand CloseNodePropertiesWindowCommand
        {
            get; private set;
        }
        public RelayCommand RemoveNodeSpawnPointCommand
        {
            get; private set;
        }
        public RelayCommand NextNodeCommand
        {
            get; private set;
        }
        public RelayCommand PrevNodeCommand
        {
            get; private set;
        }
        public RelayCommand DeselectCommand
        {
            get; private set;
        }
        public RelayCommand ToggleGridCommand
        {
            get; private set;
        }
        public RelayCommand<CancelEventArgs> BeforeCloseCommand
        {
            get; private set;
        }
        public RelayCommand<string> OpenRecentFileCommand
        {
            get; private set;
        }
        public RelayCommand RefreshErrorCommand
        {
            get; private set;
        }
        public RelayCommand<string> SetEmptyBackgroundCommand
        {
            get; private set;
        }

        #endregion

        #region Command Implementations

        #region File Commands

        #region New File

        public void NewFileCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsDirty || _lastFilePath != null;
        }

        public void NewFile(object sender, ExecutedRoutedEventArgs e)
        {
            _fileManager.newFile();
        }

        #endregion

        #region Open File

        public void OpenFile(object sender, ExecutedRoutedEventArgs e)
        {
            _fileManager.openFile();
        }

        public void OpenRecentFile(string filepath)
        {
            _fileManager.openFileFromRecent(filepath);
        }

        #endregion

        #region Save File

        public void SaveFileCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = FeaturedNode != null && NodeSpawnPoint != null;
        }
        public void SaveFile(object sender, ExecutedRoutedEventArgs e)
        {
            _fileManager.save();
        }
        public void SaveAs(object sender, ExecutedRoutedEventArgs e)
        {
            _fileManager.saveAs();
        }

        #endregion

        #region Set Background

        public void SetBackground(object sender, ExecutedRoutedEventArgs e)
        {
            _fileManager.setBackGroundFromImage();
        }

        public void SetEmptyBackground(string parameter)
        {
            var size = Size.Parse(parameter);
            _fileManager.setEmptyBackGround(size);
        }

        #endregion

        #region Save Confirmation Before Close

        public void BeforeClose(CancelEventArgs e)
        {
            e.Cancel = !_fileManager.confirmSave();
        }

        #endregion
        
        #endregion

        #region Undo / Redo

        public void UndoCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = CrazyMarble_MapEditor.Util.CommandManager.IsUndoPossible();
        }
        public void Undo(object sender, ExecutedRoutedEventArgs e)
        {
            CrazyMarble_MapEditor.Util.CommandManager.Undo();
        }
        public void RedoCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = CrazyMarble_MapEditor.Util.CommandManager.IsRedoPossible();
        }
        public void Redo(object sender, ExecutedRoutedEventArgs e)
        {
            CrazyMarble_MapEditor.Util.CommandManager.Redo();
        }

        #endregion

        #region Edit Nodes

        public void NewNodeCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BackgroundSize.Width != 0 && BackgroundSize.Height != 0;
        }
        public void NewNode(object sender, ExecutedRoutedEventArgs e)
        {
            _newNode();
        }
        private void _newNode(NodeViewModel source = null)
        {
            if (_nodes.Count > MAX_NODECOUNT)
            {
                _UIservice.MsgBox(
                    string.Format(Resources.MsgNodeReachedLimitText, MAX_NODECOUNT),
                    Resources.MsgNodeReachedLimitTitle,
                    MessageBoxImage.Error,
                    MessageBoxButton.OK);
                return;
            }

            CrazyMarble_MapEditor.Util.CommandManager.ExecuteNewCommand(
                new NewNodeCommand(this, source, NodeSpawnPoint));
            if (NodeSpawnPoint != null)
            {
                NodeSpawnPoint = null;
            }

            // Check whether previous node is connected to somewhere,
            // and connect automatically to newly created node. 
            if (!IsIgnoringAutoConnect && Nodes.Count > 1)
            {
                if (!Nodes[Nodes.Count - 2].Next.HasValue)
                {
                    foreach (IConnector e in Edges)
                    {
                        if (e.Start == Nodes[Nodes.Count - 2] && e.End == null)
                        {
                            e.Connect(Nodes[Nodes.Count - 1]);
                        }
                    }
                }
            }

            FeaturedNode = Nodes[Nodes.Count - 1];
            IsNodePropertiesWindowOpen = true;
        }

        public void MoveNodeCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = FeaturedNode != null && NodeSpawnPoint != null;
        }
        public void MoveNode(object sender, ExecutedRoutedEventArgs e)
        {
            var node = FeaturedNode as IMoveable;
            var disp = new Vector(NodeSpawnPoint.Value.X - node.X, NodeSpawnPoint.Value.Y - node.Y);
            node.Move(node, disp);
            NodeSpawnPoint = null;
        }

        public void CopyNodeCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = FeaturedNode != null;
        }
        public void CopyNode(object sender, ExecutedRoutedEventArgs e)
        {
            _copiedNode = FeaturedNode.Copy();
        }

        public void PasteNodeCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _copiedNode != null;
        }
        public void PasteNode(object sender, ExecutedRoutedEventArgs e)
        {
            _newNode(_copiedNode);
        }

        public void DeleteNodeCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = FeaturedNode != null;
        }
        public void DeleteNode(object sender, ExecutedRoutedEventArgs e)
        {
            CrazyMarble_MapEditor.Util.CommandManager.ExecuteNewCommand(new DeleteNodeCommand(this, _featuredNode));
        }

        #endregion

        #region Edit Edges

        public void CreateEdge(int ID, EdgeType type)
        {
            CrazyMarble_MapEditor.Util.CommandManager.ExecuteNewCommand(
                new CreateEdgeCommand(_edges, _nodes[ID], type));
        }
        public void DeleteEdge(IConnector edge)
        {
            CrazyMarble_MapEditor.Util.CommandManager.ExecuteNewCommand(
                new DeleteEdgeCommand(_edges, edge as EdgeViewModel));
        }

        #endregion

        #region Selection / NodeSpawnPoint / Grid / Popup management

        public void SelectNode(int ID)
        {
            CrazyMarble_MapEditor.Util.CommandManager.IsJournalEnabled = false;
            FeaturedNode = Nodes[ID];
            IsNodePropertiesWindowOpen = true;
            CrazyMarble_MapEditor.Util.CommandManager.IsJournalEnabled = true;
        }

        public void OnNodeSpawnPointMessage(NodeSpawnPointMessage msg)
        {
            switch (msg.Type)
            {
                case NodeSpawnPointMessage.CommandType.Create:
                    break;
                case NodeSpawnPointMessage.CommandType.Delete:
                    NodeSpawnPoint = null;
                    break;
                default:
                    break;
            }
        }

        public void ClosePropertiesWindow()
        {
            IsNodePropertiesWindowOpen = false;
        }

        public void NextNode()
        {
            if (FeaturedNode != null)
            {
                SelectNode((FeaturedNode.ID + 1) % Nodes.Count);
            }
        }
        public void PrevNode()
        {
            if (FeaturedNode != null)
            {
                SelectNode((FeaturedNode.ID + (Nodes.Count-1)) % Nodes.Count);
            }
        }

        public void Deselect(object sender, ExecutedRoutedEventArgs e)
        {
            ClosePropertiesWindow();
        }

        public void RemoveNodeSpawnPoint(object sender, ExecutedRoutedEventArgs e)
        {
            NodeSpawnPoint = null;
        }

        public void ToggleGrid()
        {
            Grid.IsEnabled = !Grid.IsEnabled;
        }

        #endregion

        #region Open Windows

        public void OpenStatistics(object sender, ExecutedRoutedEventArgs e)
        {
            _UIservice.ShowStatisticsWindow();
        }

        public void OpenAbout(object sender, ExecutedRoutedEventArgs e)
        {
            _UIservice.ShowAboutWindow();
        }

        public void OpenBeforeSave(object sender, ExecutedRoutedEventArgs e)
        {
            verify();
            _UIservice.ShowErrorWindow();
        }

        #endregion

        #region Refreshes

        public void RefreshErrorList()
        {
            verify();
        }

        #endregion

        #endregion

        #region Undoable Command Implementations

        #region Create Node
        public class NewNodeCommand : UndoableCommand
        {
            private MapViewModel _map;
            private NodeViewModel _createdNode;
            private EdgeViewModel _createdDefaultEdge;


            private BindingList<NodeViewModel> _nodes;
            private BindingList<EdgeViewModel> _edges;

            public NewNodeCommand(MapViewModel map, NodeViewModel source = null, Point? pos = null)
            {
                _map = map;
                _nodes = map._nodes;
                _edges = map._edges;

                // Create one if does not exist
                if (pos != null)
                {
                    if (source != null)
                    {
                        _createdNode = new NodeViewModel(source, _nodes.Count, pos.Value.X, pos.Value.Y);
                    }
                    else
                    {
                        _createdNode = new NodeViewModel(_nodes.Count, pos.Value.X, pos.Value.Y);
                    }
                }
                else
                {
                    if (source != null)
                    {
                        _createdNode = new NodeViewModel(
                            source,
                            _nodes.Count);
                    }
                    else
                    {
                        _createdNode = new NodeViewModel(_nodes.Count);
                    }
                }

                _createdDefaultEdge = new EdgeViewModel(_createdNode, EdgeType.Next);
            }

            public override string Discription
            {
                get { return Resources.MenuCreateNode; }
            }

            public override void Execute()
            {
                //_map.Nodes.Insert(_createdNode.ID, _createdNode);
                _nodes.Insert(_createdNode.ID, _createdNode);
                _edges.Add(_createdDefaultEdge);
            }

            public override void UnExecute()
            {
                _map._updateNodePropertiesWindow(_createdNode.ID, NodeChangedMessage.NodeState.Deleted);
                //_map.Nodes.RemoveAt(_createdNode.ID);
                _nodes.RemoveAt(_createdNode.ID);
                _edges.Remove(_createdDefaultEdge);
            }
        }
        #endregion

        #region Delete Node
        public class DeleteNodeCommand : UndoableCommand
        {
            private MapViewModel _map;

            private NodeViewModel _deletedNode;
            private List<IConnector> _inEdges;
            private Dictionary<IConnector, IConnectable> _outEdges;

            private BindingList<NodeViewModel> _nodes;
            private BindingList<EdgeViewModel> _edges;

            public DeleteNodeCommand(MapViewModel map, NodeViewModel node)
            {
                _map = map;
                _nodes = map._nodes;
                _edges = map._edges;

                _deletedNode = node;
                _inEdges = new List<IConnector>();
                _outEdges = new Dictionary<IConnector, IConnectable>();

                foreach (IConnector edge in node.Edges)
                {
                    if (edge.Start == node)
                    {
                        _outEdges[edge] = edge.End;
                    }
                    else
                    {
                        _inEdges.Add(edge);
                    }
                }
            }

            public override string Discription
            {
                get { return Resources.MenuDeleteNode; }
            }

            public override void Execute()
            {
                _map._updateNodePropertiesWindow(_deletedNode.ID, NodeChangedMessage.NodeState.Deleted);
                foreach (IConnector edge in _inEdges)
                {
                    edge.Disconnect(_deletedNode);
                }
                foreach (IConnector edge in _outEdges.Keys)
                {
                    if (_outEdges[edge] != null)
                    {
                        edge.Disconnect(_outEdges[edge]);
                    }
                    _edges.Remove(edge as EdgeViewModel);
                }

                _nodes.RemoveAt(_deletedNode.ID);
            }

            public override void UnExecute()
            {
                _nodes.Insert(_deletedNode.ID, _deletedNode);
                foreach (IConnector edge in _inEdges)
                {
                    edge.Connect(_deletedNode);
                }
                foreach (IConnector edge in _outEdges.Keys)
                {
                    if (_outEdges[edge] != null)
                    {
                        edge.Connect(_outEdges[edge]);
                    }
                    _edges.Add(edge as EdgeViewModel);
                }
            }
        }
        #endregion

        #region Create Edge
        public class CreateEdgeCommand : UndoableCommand
        {
            private BindingList<EdgeViewModel> _edges;
            private EdgeType _type;
            private NodeViewModel _node;
            private EdgeViewModel _edge;

            public CreateEdgeCommand(
                BindingList<EdgeViewModel> edges, NodeViewModel node, EdgeType type)
            {
                _edges = edges;
                _node = node;
                _type = type;
            }

            public override string Discription
            {
                get { return Resources.CommandCreateEdge; }
            }

            public override void Execute()
            {
                if (_edge == null)
                {
                    _edge = new EdgeViewModel(_node, _type);
                }
                else
                {
                    _node.RegisterConnector(_edge);
                }
                _edges.Add(_edge);
            }

            public override void UnExecute()
            {
                _node.UnregisterConnector(_edge);
                _edges.Remove(_edge);
            }
        }
        #endregion

        #region Delete Edge
        public class DeleteEdgeCommand : UndoableCommand
        {
            private BindingList<EdgeViewModel> _edges;
            private EdgeViewModel _edge;

            private IConnectable _src, _dest;

            public DeleteEdgeCommand(BindingList<EdgeViewModel> edges, EdgeViewModel edge)
            {
                _edges = edges;
                _edge = edge;
                _src = (edge as IConnector).Start;
                _dest = (edge as IConnector).End;
            }

            public override string Discription
            {
                get { return Resources.CommandDeleteEdge; }
            }

            public override void Execute()
            {
                if (_dest != null)
                {
                    _edge.Disconnect(_dest);
                }

                _src.UnregisterConnector(_edge);
                _edges.Remove(_edge);
            }

            public override void UnExecute()
            {
                _src.RegisterConnector(_edge);
                if (_dest != null)
                {
                    _edge.Connect(_dest);
                }
                _edges.Add(_edge);
            }
        }
        #endregion

        #endregion
    }

    #region I/O Mode
    public enum IOMode
    {
        Read, Write
    }
    #endregion
}