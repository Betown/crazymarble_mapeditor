﻿using CrazyMarble_MapEditor.Behavior;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CrazyMarble_MapEditor.ViewModel
{
    public class NodeSpawnPointViewModel : ViewModelBase, IMoveable
    {
        #region Constructor
        public NodeSpawnPointViewModel(IMoveable spawnElement, double X, double Y)
        {
            _spawnElement = spawnElement;
            DeleteNodeSpawnPointCommand = new RelayCommand(DeleteNodeSpawnPoint);
            this.X = (int)Math.Round(X) - (int)Math.Round(Width) / 2;
            this.Y = (int)Math.Round(Y) - (int)Math.Round(Height) / 2;
        }
        #endregion

        #region Members
        private IMoveable _spawnElement;
        private int _X, _Y;
        private double _width = 100, _height = 100;
        #endregion

        #region Properties
        public int ZPriority { get { return 0; } }

        public double Width
        {
            get { return _width; }
            set {
                _width = value;
                RaisePropertyChanged("Width");
            }
        }

        public double Height
        {
            get { return _height; }
            set
            {
                _height = value;
                RaisePropertyChanged("Height");
            }
        }

        public int X
        {
            get { return _X; }
            set
            {
                _X = value;
                RaisePropertyChanged("X");
            }
        }

        public int Y
        {
            get { return _Y; }
            set
            {
                _Y = value;
                RaisePropertyChanged("Y");
            }
        }

        public Size SpawnElementSize
        {
            get { return new Size(_spawnElement.Width, _spawnElement.Height); }
        }

        public IMoveable SpawnElement
        {
            get { return _spawnElement; }
            set
            {
                _spawnElement = value;
                RaisePropertyChanged("SpawnElement");
                RaisePropertyChanged("SpawnPoint");
            }
        }

        public Point SpawnPoint
        {
            get
            {
                var x = X + Width / 2 - SpawnElement.Width / 2;
                var y = Y + Height / 2 - SpawnElement.Height / 2;
                return new Point(x, y);
            }
        }

        public RelayCommand DeleteNodeSpawnPointCommand
        {
            get; private set;
        }
        #endregion

        #region Commands
        public void DeleteNodeSpawnPoint()
        {
            MessengerInstance.Send<NodeSpawnPointMessage>(
                new NodeSpawnPointMessage { Type = NodeSpawnPointMessage.CommandType.Delete });
        }
        #endregion

        #region IMoveable Members
        double IMoveable.X
        {
            get { return _X; }
        }

        double IMoveable.Y
        {
            get { return _Y; }
        }
        public Type DataType
        {
            get { return typeof(NodeSpawnPointViewModel); }
        }

        public void OnMoveStart()
        {
        }

        public void Move(object target, System.Windows.Vector displacement)
        {
            X += (int)Math.Round(displacement.X);
            Y += (int)Math.Round(displacement.Y);
        }
        #endregion
    }
}
