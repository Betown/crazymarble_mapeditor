﻿using CrazyMarble_MapEditor.Properties;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrazyMarble_MapEditor.ViewModel
{
    public class ZoomViewModel : ViewModelBase
    {
        public ZoomViewModel()
        {
            _zoom = 1;
            _minZoom = 0.25;
            _maxZoom = 4;
            ZoomCommand = new RelayCommand<string>(ZoomTo);
            AutoZoomCommand = new RelayCommand(AutoZoomOn);
        }

        #region Members
        private double _zoom;
        private bool _autoZoom;
        private bool _autoZooming;

        private double _minZoom;
        private double _maxZoom;

        private double _originalWidth;
        private double _originalHeight;
        private double _viewportWidth;
        private double _viewportHeight;
        #endregion

        #region Properties
        public double Zoom
        {
            get { return _zoom; }
            set
            {
                if (_zoom == value) return;

                _zoom = Math.Max(_minZoom, Math.Min(_maxZoom, value));
                if (!_autoZooming)
                {
                    AutoZoom = false;
                }
                RaisePropertyChanged("Zoom");
                RaisePropertyChanged("ZoomedWidth");
                RaisePropertyChanged("ZoomedHeight");
                RaisePropertyChanged("ZoomStatusString");
            }
        }

        public bool AutoZoom
        {
            get { return _autoZoom; }
            set 
            { 
                _autoZoom = value;
                _updateAutoZoom();
                RaisePropertyChanged("AutoZoom");
                RaisePropertyChanged("ZoomStatusString");
            }
        }

        public double MinZoom
        {
            get { return _minZoom; }
            set 
            {
                _minZoom = value;
                _updateAutoZoom();
                RaisePropertyChanged("MinZoom");
            }
        }

        public double MaxZoom
        {
            get { return _maxZoom; }
            set 
            {
                _maxZoom = value;
                _updateAutoZoom();
                RaisePropertyChanged("MaxZoom");
            }
        }

        // Original/Viewport Width, Height are only used for auto zooming.
        // It is not recommended to write values to those properties without Oneway binding.
        public double OriginalWidth
        {
            get { return _originalWidth; }
            set 
            {
                _originalWidth = value;
                _updateAutoZoom();
                RaisePropertyChanged("OriginalWidth");
                RaisePropertyChanged("ZoomedWidth");
            }
        }

        public double OriginalHeight
        {
            get { return _originalHeight; }
            set 
            {
                _originalHeight = value;
                _updateAutoZoom();
                RaisePropertyChanged("OriginalHeight");
                RaisePropertyChanged("ZoomedHeight");
            }
        }

        public double ViewportWidth
        {
            get { return _viewportWidth; }
            set
            {
                _viewportWidth = value;
                _updateAutoZoom();
                RaisePropertyChanged("ViewportWidth");
            }
        }

        public double ViewportHeight
        {
            get { return _viewportHeight; }
            set 
            {
                _viewportHeight = value;
                _updateAutoZoom();
                RaisePropertyChanged("ViewportHeight");
            }
        }

        public double ZoomedWidth { get { return _zoom * OriginalWidth; } }
        public double ZoomedHeight { get { return _zoom * OriginalHeight; } }

        public string ZoomStatusString 
        { 
            get
            {
                if (AutoZoom) return Resources.ZoomFitWindow;
                return string.Format("{0:P0}", Zoom);
            }
        }
        #endregion

        #region Commands
        public RelayCommand<string> ZoomCommand
        {
            get;
            private set;
        }
        public RelayCommand AutoZoomCommand
        {
            get;
            private set;
        }

        public void ZoomTo(string zoom)
        {
            AutoZoom = false;
            Zoom = double.Parse(zoom);
        }
        public void AutoZoomOn()
        {
            AutoZoom = true;
        }
        #endregion

        #region Auto Zooming
        private void _updateAutoZoom()
        {
            if (!AutoZoom) return;

            double widthRatio = _viewportWidth / _originalWidth;
            double heightRatio = _viewportHeight / _originalHeight;

            _autoZooming = true;
            Zoom = Math.Min(widthRatio, heightRatio);
            _autoZooming = false;
        }
        #endregion
    }
}
