﻿using CrazyMarble_MapEditor.Behavior;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrazyMarble_MapEditor.ViewModel
{
    #region Messages
    public class NodeSelectMessage
    {
        public int ID { get; set; }
    }

    public class NodeChangedMessage
    {
        public int ID { get; set; }
        public enum NodeState { Moving, Moved, Deleted }
        public NodeState State { get; set; }
    }

    public class EdgeCreateMessage
    {
        public int ID { get; set; }
        public EdgeType Type { get; set; }
    }

    public class EdgeDeleteMessage
    {
        public IConnector Edge { get; set; }
    }

    public class NodeSpawnPointMessage
    {
        public enum CommandType { Create, Delete }
        public CommandType Type;
    }
    #endregion
}
