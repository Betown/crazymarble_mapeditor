﻿using CrazyMarble_MapEditor.Model;
using CrazyMarble_MapEditor.Properties;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Threading;

namespace CrazyMarble_MapEditor.ViewModel
{
    public class StatisticsViewModel : ViewModelBase
    {
        #region Constructor
        static StatisticsViewModel()
        {
            _nodeStatisticsBase = new Dictionary<string, StatisticsLegendElement>();
            var legends = NodeViewModel.SpaceTypeNames.Zip(
                NodeViewModel.SpaceTypes, (originalName, name) =>
                    new StatisticsLegendElement
                    {
                        IconSource = string.Format("../Image/Space/Statistics/StatisticsSpace_{0}.png", originalName),
                        Name = name
                    });

            foreach (StatisticsLegendElement elem in legends)
            {
                _nodeStatisticsBase.Add(elem.Name, elem);
            }

            _edgeStatisticsBase = new Dictionary<EdgeType, StatisticsLegendElement>();
            foreach (KeyValuePair<EdgeType, Brush> entry in EdgeViewModel.EdgeColors)
            {
                _edgeStatisticsBase.Add(entry.Key,
                    new StatisticsLegendElement
                    {
                        Color = entry.Value,
                        Name = (string)typeof(Resources).GetProperty(typeof(EdgeType).Name + entry.Key.ToString(), 
                        BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy).GetValue(null)
                    });
            }

        }
        public StatisticsViewModel(MapViewModel map)
        {
            _map = map;
            _zoomManager = new ZoomViewModel();
            Update();
            UpdateCommand = new RelayCommand(Update);
        }
        #endregion

        #region Member
        private static Dictionary<string, StatisticsLegendElement> _nodeStatisticsBase;
        private static Dictionary<EdgeType, StatisticsLegendElement> _edgeStatisticsBase;

        private MapViewModel _map;
        private Dictionary<string, StatisticsLegendElement> _nodeStatistics;
        private Dictionary<EdgeType, StatisticsLegendElement> _edgeStatistics;
        private Characteristics _mapCharacteristics;

        private ZoomViewModel _zoomManager;
        #endregion

        #region Properties
        public MapViewModel Map
        {
            get { return _map; }
        }
        public Dictionary<string, StatisticsLegendElement> NodeStatistics
        {
            get { return _nodeStatistics; }
            private set { 
                _nodeStatistics = value;
                RaisePropertyChanged("NodeStatistics");
            }
        }
        public Dictionary<EdgeType, StatisticsLegendElement> EdgeStatistics
        {
            get { return _edgeStatistics; }
            private set
            {
                _edgeStatistics = value;
                RaisePropertyChanged("EdgeStatistics");
            }
        }
        public Characteristics MapCharacteristics
        {
            get { return _mapCharacteristics; }
            private set 
            { 
                _mapCharacteristics = value;
                RaisePropertyChanged("MapCharacteristics");
            }
        }

        public ZoomViewModel Zoom
        {
            get { return _zoomManager; }
        }
        #endregion

        #region Commands
        public RelayCommand UpdateCommand
        {
            get;
            private set;
        }

        public void BeforeInitialAutoZoom()
        {
            _zoomManager.MinZoom = .0;
            _zoomManager.MaxZoom = .5;
            _zoomManager.Zoom = .0;
        }

        public void InitialAutoZoom()
        {
            _zoomManager.MinZoom = 0.05;
            _zoomManager.AutoZoom = true;
        }

        public void Update()
        {
            var nodeLegends = _nodeStatisticsBase.ToDictionary(
                entry => String.Copy(entry.Key),
                entry => new StatisticsLegendElement(entry.Value));

            getNodeStatistics(nodeLegends);

            var edgeLegends = _edgeStatisticsBase.ToDictionary(
                entry => entry.Key,
                entry => new StatisticsLegendElement(entry.Value));

            getEdgeStatistics(edgeLegends);
            getCharacteristics(nodeLegends, edgeLegends);
        }

        private void getCharacteristics(Dictionary<string, StatisticsLegendElement> nodeLegends, Dictionary<EdgeType, StatisticsLegendElement> edgeLegends)
        {
            var mapChara = new Characteristics();
            double money = nodeLegends[Node.SpaceType.Shop.ToString()].Count * 10;
            money += nodeLegends[Node.SpaceType.Start.ToString()].Count * 20;
            money += nodeLegends[Node.SpaceType.Bank.ToString()].Count * 20;
            mapChara.Money = (int)money;

            double draw = nodeLegends[Node.SpaceType.Chest.ToString()].Portion * 2;
            draw += nodeLegends[Node.SpaceType.Shop.ToString()].Count * 10;
            draw += nodeLegends[Node.SpaceType.HouseOfSage.ToString()].Count * 20;
            mapChara.Draw = (int)draw;

            double randomness = nodeLegends[Node.SpaceType.Event.ToString()].Portion * 3;
            randomness += nodeLegends[Node.SpaceType.Chest.ToString()].Portion;
            randomness += nodeLegends[Node.SpaceType.HouseOfSage.ToString()].Portion;
            mapChara.Randomness = (int)randomness;

            double complexity = edgeLegends[EdgeType.NextIfArrived].Count * 10;
            complexity += edgeLegends[EdgeType.JumpTo].Count * 5;
            complexity -= nodeLegends[Node.SpaceType.Warp.ToString()].Count * 5;
            if (Map.Nodes.Count < 20)
            {
                complexity -= (20 - Map.Nodes.Count) * 2;
            }
            else if (Map.Nodes.Count > 40)
            {
                complexity += (40 - Map.Nodes.Count) * 2;
            }
            mapChara.Complexity = (int)complexity;

            MapCharacteristics = mapChara;
        }

        private void getEdgeStatistics(Dictionary<EdgeType, StatisticsLegendElement> edgeLegends)
        {
            foreach (EdgeViewModel edge in Map.Edges)
            {
                edgeLegends[edge.Type].Count++;
            }

            foreach (StatisticsLegendElement elem in edgeLegends.Values)
            {
                elem.Portion = (double)elem.Count / Map.Edges.Count;
            }
            EdgeStatistics = edgeLegends;
        }

        private void getNodeStatistics(Dictionary<string, StatisticsLegendElement> nodeLegends)
        {
            foreach (NodeViewModel node in Map.Nodes)
            {
                nodeLegends[node.Type].Count++;
            }

            foreach (StatisticsLegendElement elem in nodeLegends.Values)
            {
                elem.Portion = (double)elem.Count / Map.Nodes.Count;
            }
            NodeStatistics = nodeLegends;
        }
        #endregion
    }
}
