﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace CrazyMarble_MapEditor.ViewModel
{
    public class GridViewModel : ViewModelBase
    {
        public GridViewModel()
        {
            IsEnabled = true;
            Size = new Size(32.0, 32.0);
            EnableGridCommand = new RelayCommand<string>(EnableGrid);
        }

        #region Members
        private bool _isEnabled;
        private Size _size;
        #endregion

        #region Properties
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; RaisePropertyChanged("IsEnabled"); }
        }
        public Size Size
        {
            get { return _size; }
            set { _size = value; RaisePropertyChanged("Size"); }
        }
        #endregion

        #region Commands
        public RelayCommand<string> EnableGridCommand
        {
            get;
            private set;
        }

        public void EnableGrid(string size)
        {
            var values = size.Split(',');
            Size = new Size(double.Parse(values[0]), double.Parse(values[1]));
            IsEnabled = true;
        }
        #endregion
    }
}
