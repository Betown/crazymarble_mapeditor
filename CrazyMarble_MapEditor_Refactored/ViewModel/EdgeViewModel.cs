﻿using CrazyMarble_MapEditor.Behavior;
using CrazyMarble_MapEditor.Properties;
using CrazyMarble_MapEditor.Util;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace CrazyMarble_MapEditor.ViewModel
{
    public class EdgeViewModel : ObservableObject, IMoveable, IConnector
    {
        #region Constructor
        static EdgeViewModel()
        {
            _edgeColor = new Dictionary<EdgeType, Brush>();
            _defaultAngle = new Dictionary<EdgeType, double>();

            foreach (EdgeType type in Enum.GetValues(typeof(EdgeType)))
            {
                Brush edgeColor = null;
                double defaultAngle = 0;
                switch (type)
                {
                    case EdgeType.Next:
                        edgeColor = Brushes.Black;
                        defaultAngle = 7 * Math.PI / 4;
                        break;
                    case EdgeType.NextIfArrived:
                        edgeColor = Brushes.Red;
                        defaultAngle = 3 * Math.PI / 4;
                        break;
                    case EdgeType.JumpTo:
                        edgeColor = Brushes.RoyalBlue;
                        defaultAngle = 5 * Math.PI / 4;
                        break;
                }
                _edgeColor[type] = edgeColor;
                _defaultAngle[type] = defaultAngle;
            }

        }
        public EdgeViewModel(IConnectable startNode, EdgeType type)
        {
            _X1 = startNode.X;
            _Y1 = startNode.Y;
            _length = startNode.Width * 3 / 4;
            _startWidth = startNode.Width;
            _startHeight = startNode.Height;
            _startNode = startNode;

            _type = type;
            _angle = _defaultAngle[_type];
            EdgeColor = _edgeColor[_type];
            startNode.RegisterConnector(this);
            Update();
        }
        #endregion

        #region Members
        private static Dictionary<EdgeType, Brush> _edgeColor;
        private static Dictionary<EdgeType, double> _defaultAngle;

        private IConnectable _startNode;
        private IConnectable _endNode;

        private EdgeType _type;
        private double _angle;
        private double _length;

        private double _startWidth;
        private double _startHeight;
        private double _endWidth;
        private double _endHeight;

        private double _X1;
        private double _X2;
        private double _Y1;
        private double _Y2;
        #endregion

        #region Properties
        public int ZPriority { get { return -1; } }

        public double X1 { get; set; }
        public double X2 { get; set; }
        public double Y1 { get; set; }
        public double Y2 { get; set; }

        public double X { get; set; }
        public double Y { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Angle { get; set; }

        public Brush EdgeColor { get; set; }
        public EdgeType Type { get { return _type; } }
        #endregion

        #region Properties
        public static Dictionary<EdgeType, Brush> EdgeColors
        {
            get { return _edgeColor; }
        }
        #endregion

        #region IMoveable Members
        double IMoveable.X
        {
            get { return X; }
        }
        double IMoveable.Y
        {
            get { return Y; }
        }
        double IMoveable.Width
        {
            get { return Width; }
        }
        double IMoveable.Height
        {
            get { return Height; }
        }

        Type IMoveable.DataType
        {
            get
            {
                return typeof(EdgeViewModel);
            }
        }

        public void OnMoveStart()
        {
        }

        public void Move(object data, Vector disp)
        {
            X += (int)Math.Round(disp.X);
            Y += (int)Math.Round(disp.Y);
        }
        #endregion

        #region IConnector Members
        public void Connect(IConnectable target)
        {
            CommandManager.ExecuteNewCommand(new ConnectNodeCommand(this, target, null));
        }
        private void _connect(IConnectable target)
        {
            if (_endNode != null)
            {
                _endNode.OnDisconnect(this, _startNode);
            }
            _endNode = target;
            _startNode.OnConnect(this, _endNode);
            _endNode.OnConnect(this, _startNode);
            Update();
        }

        public void OnConnectFailed(Vector disp)
        {
            CommandManager.ExecuteNewCommand(new ConnectNodeCommand(this, null, Math.Atan2(-disp.Y, disp.X)));
        }
        public void Disconnect(IConnectable target)
        {
            _endNode.OnDisconnect(this, _startNode);
            _startNode.OnDisconnect(this, _endNode);
            _endNode = null;
            Update();
        }

        public void Update()
        {
            _X1 = _startNode.X;
            _Y1 = _startNode.Y;

            _startWidth = _startNode.Width;
            _startHeight = _startNode.Height;

            if (_endNode != null)
            {
                _X2 = _endNode.X;
                _Y2 = _endNode.Y;

                _endWidth = _endNode.Width;
                _endHeight = _endNode.Height;
            }

            X1 = _startWidth / 2 + _X1;
            Y1 = _startHeight / 2 + _Y1;

            if (_endNode == null)
            {
                X2 = X1 + _length * Math.Cos(_angle);
                Y2 = Y1 + _length * -Math.Sin(_angle);
            }
            else
            {
                X2 = _endWidth / 2 + _X2;
                Y2 = _endHeight / 2 + _Y2;
            }

            double dX = X2 - X1;
            double dY = Y2 - Y1;

            Width = Math.Abs(dX);
            Height = Math.Abs(dY);

            X = Math.Min(X1, X2);
            Y = Math.Min(Y1, Y2);

            if (_endNode != null)
            {
                _angle = Math.Atan2(-dY, dX);
            }

            X1 = X1 - X;
            Y1 = Y1 - Y;
            X2 = X2 - X;
            Y2 = Y2 - Y;

            X1 += _startWidth / 2 * Math.Cos(_angle);
            Y1 += _startHeight / 2 * -Math.Sin(_angle);

            if (_endNode != null)
            {
                X2 -= _endWidth / 2 * Math.Cos(_angle);
                Y2 -= _endHeight / 2 * -Math.Sin(_angle);
            }

            Angle = _angle;

            RaisePropertyChanged("X");
            RaisePropertyChanged("Y");
            RaisePropertyChanged("Width");
            RaisePropertyChanged("Height");
            RaisePropertyChanged("Angle");

            RaisePropertyChanged("X1");
            RaisePropertyChanged("X2");
            RaisePropertyChanged("Y1");
            RaisePropertyChanged("Y2");
        }

        public IConnectable Start
        {
            get { return _startNode; }
        }

        public IConnectable End
        {
            get { return _endNode; }
        }

        Type IConnector.ConnectorType
        {
            get { return typeof(EdgeViewModel); }
        }
        #endregion

        #region Undoable Command Implementations

        #region Connect
        public class ConnectNodeCommand : UndoableCommand
        {
            private EdgeViewModel _edge;

            private IConnectable _prevTarget;
            private IConnectable _target;

            private double? _prevAngle;
            private double? _angle;


            public ConnectNodeCommand(EdgeViewModel edge, IConnectable target, double? angle)
            {
                _edge = edge;
                _target = target;
                _angle = angle;

                // Save previous state
                if (_edge._endNode != null)
                {
                    _prevAngle = null;
                }
                else
                {
                    _prevAngle = _edge._angle;
                }

                _prevTarget = _edge._endNode;
            }

            public override string Discription
            {
                get
                {
                    if (_prevTarget == null && _target == null)
                    {
                        return Resources.CommandChangeEdgeAngle;
                    }
                    else if (_prevTarget != null && _target == null)
                    {
                        return Resources.CommandDisconnect;
                    }
                    else
                    {
                        return Resources.CommandConnect;
                    }
                }
            }
            
            public override void Execute()
            {
                // Connect Failed
                if (_target == null)
                {
                    _edge._angle = _angle.Value;
                    if (_prevTarget != null)
                    {
                        _edge.Disconnect(_prevTarget);
                    }
                    else
                    {
                        _edge.Update();
                    }
                }
                else
                {
                    _edge._connect(_target);
                }

            }

            public override void UnExecute()
            {
                if (_prevTarget == null)
                {
                    _edge._angle = _prevAngle.Value;
                    if (_target != null)
                    {
                        _edge.Disconnect(_target);
                    }
                    else
                    {
                        _edge.Update();
                    }
                }
                else
                {
                    _edge._connect(_prevTarget);
                }
            }
        }
        #endregion

        #endregion
    }

    public enum EdgeType
    {
        Next = 0,
        NextIfArrived = 1,
        JumpTo = 2
    }
}
