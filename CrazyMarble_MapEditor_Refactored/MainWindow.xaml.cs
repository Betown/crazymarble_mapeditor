﻿using CrazyMarble_MapEditor.Properties;
using CrazyMarble_MapEditor.View;
using CrazyMarble_MapEditor.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CrazyMarble_MapEditor
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window, IUIService, IRecentFileService
    {
        private MapViewModel _vm;
        private Window _errorListWindow;
        private Window _statisticsWindow;

        #region Constructor and Initialization

        public MainWindow()
        {
            DataContext = new MapViewModel(this, this);
            InitializeComponent();
            _vm = DataContext as MapViewModel;
        }

        #endregion

        #region IUIService Members
        public MessageBoxResult MsgBox(string str, string caption,
            MessageBoxImage img, MessageBoxButton options)
        {
            return MessageBox.Show(str, caption, options, img);
        }

        public string FileDialog(string defaultName, string defaultExt, string filter, IOMode ioMode)
        {
            Microsoft.Win32.FileDialog dlg;
            switch (ioMode)
            {
                case IOMode.Read:
                    dlg = new Microsoft.Win32.OpenFileDialog();
                    dlg.Filter = filter;
                    break;
                case IOMode.Write:
                    dlg = new Microsoft.Win32.SaveFileDialog();
                    dlg.DefaultExt = ".json"; // Default file extension
                    dlg.Filter = "JavaScript Object Notation File (.json)|*.json";
                    break;
                default:
                    dlg = null;
                    break;
            }

            if (dlg != null)
            {
                dlg.FileName = defaultName;
                dlg.DefaultExt = defaultExt;
                dlg.Filter = filter;
                if (dlg.ShowDialog() == true)
                {
                    return dlg.FileName;
                }
            }
            return null;
        }

        public void ShowErrorWindow()
        {
            if (_errorListWindow == null || !_errorListWindow.IsLoaded)
            {
                _errorListWindow = new ErrorListWindow(DataContext as MapViewModel);
                _errorListWindow.Owner = this;
                _errorListWindow.Show();
            }
            else
            {
                _errorListWindow.Focus();
            }
        }

        public void ShowStatisticsWindow()
        {
            if (_statisticsWindow == null || !_statisticsWindow.IsLoaded)
            {
                _statisticsWindow = new StatisticsWindow(DataContext as MapViewModel);
                _statisticsWindow.Owner = this;
                _statisticsWindow.Show();
            }
            else
            {
                _errorListWindow.Focus();
            }
        }

        public void ShowAboutWindow()
        {
            var about = new AboutWindow();
            about.ShowDialog();
        }

        public IDisposable BusyCursor()
        {
            return new WaitCursor();
        }
        public class WaitCursor : IDisposable
        {
            private Cursor _previousCursor;

            public WaitCursor()
            {
                _previousCursor = Mouse.OverrideCursor;

                Mouse.OverrideCursor = Cursors.Wait;
            }

            #region IDisposable Members
            public void Dispose()
            {
                Mouse.OverrideCursor = _previousCursor;
            }
            #endregion
        }

        #endregion

        #region IRecentFileService
        public void AddHistory(string filePath)
        {
            RecentFileList.InsertFile(filePath);
        }

        public bool RemoveHistory(string filePath)
        {
            if (RecentFileList.RecentFiles.Contains(filePath))
            {
                RecentFileList.RemoveFile(filePath);
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Preventing AutoConnect by Pressing Shift
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                _vm.IsIgnoringAutoConnect = true;
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                _vm.IsIgnoringAutoConnect = false;
            }
        }
        #endregion

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        // Since EventToCommand does not work for this event,
        // resorting to make this listener.
        private void RecentFileList_MenuClick(object sender, Common.RecentFileList.MenuClickEventArgs e)
        {
            _vm.OpenRecentFileCommand.Execute(e.Filepath);
        }

        // Stop moving command when switch(windows start menu/task change, etc.)
        private void Window_Deactivated(object sender, EventArgs e)
        {
            Behavior.FrameworkElementMoveBehavior.StopMove();
        }

    }

    #region User Interface Service
    public interface IUIService
    {
        MessageBoxResult MsgBox(
            string str, 
            string caption, 
            MessageBoxImage img, 
            MessageBoxButton options);

        string FileDialog(
            string defaultName,
            string defaultExt,
            string filter,
            IOMode ioMode);

        void ShowErrorWindow();
        void ShowStatisticsWindow();
        void ShowAboutWindow();

        IDisposable BusyCursor();
    }

    public interface IRecentFileService
    {
        void AddHistory(string filePath);
        bool RemoveHistory(string filePath);
    }
    #endregion

    public static class Command
    {
        public static readonly RoutedUICommand CreateNode =
            new RoutedUICommand(Resources.MenuCreateNode, "Create a Node", typeof(MainWindow));
        public static readonly RoutedUICommand SetBackground =
            new RoutedUICommand(Resources.MenuSetBackground, "Set Background", typeof(MainWindow));
        public static readonly RoutedUICommand Statistics =
            new RoutedUICommand(Resources.MenuStatistics, "See Statistics", typeof(MainWindow));
        public static readonly RoutedUICommand BeforeSave =
            new RoutedUICommand(Resources.MenuBeforeSaving, "Before saving...", typeof(MainWindow));
        public static readonly RoutedUICommand ProgramInfo =
            new RoutedUICommand(Resources.MenuProgramInfo, "About this program", typeof(MainWindow));

        public static readonly RoutedUICommand Deselect =
            new RoutedUICommand(Resources.MenuDeselect, "Deselect", typeof(MainWindow));
        public static readonly RoutedUICommand RemoveNodeSpawnPoint =
            new RoutedUICommand(Resources.MenuRemoveNodeSpawnPoint, "Remove selection", typeof(MainWindow));
    }

}
