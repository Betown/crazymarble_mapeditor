﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CrazyMarble_MapEditor.Util
{
    class UtilFunc
    {
        public static bool IsNumeric(object obj)
        {
            var dataType = obj.GetType();
            return (dataType == typeof(int)
            || dataType == typeof(double)
            || dataType == typeof(long)
            || dataType == typeof(short)
            || dataType == typeof(float)
            || dataType == typeof(Int16)
            || dataType == typeof(Int32)
            || dataType == typeof(Int64)
            || dataType == typeof(uint)
            || dataType == typeof(UInt16)
            || dataType == typeof(UInt32)
            || dataType == typeof(UInt64)
            || dataType == typeof(sbyte)
            || dataType == typeof(Single)
           );
        }


        // http://stackoverflow.com/questions/12076107/how-to-detect-if-any-key-is-pressed
        public static bool IsAnyKeyDown()
        {
            foreach (var v in Enum.GetValues(typeof(Key)))
            {
                if (((Key)v) != Key.None)
                {
                    if (Keyboard.IsKeyDown((Key)v))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        // http://stackoverflow.com/questions/5750722/how-to-detect-modifier-key-states-in-wpf
        public static bool IsAnySpecialKeyDown()
        {
            foreach (var v in Enum.GetValues(typeof(ModifierKeys)))
            {
                if (((ModifierKeys)v) != ModifierKeys.None)
                {
                    if (Keyboard.Modifiers.HasFlag((ModifierKeys)v))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
