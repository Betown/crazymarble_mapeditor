﻿using CrazyMarble_MapEditor.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrazyMarble_MapEditor.Util
{
    #region ID Managing BindingList
    public class NodeCollection<T> : BindingList<T> where T : NodeViewModel
    {
        protected override void InsertItem(int index, T item)
        {
            for (int i = index; i < this.Count; i++)
            {
                this[i].ID++;
            }
            base.InsertItem(index, item);
        }

        protected override void RemoveItem(int index)
        {
            for (int i = index + 1; i < this.Count; i++)
            {
                this[i].ID--;
            }
            base.RemoveItem(index);
        }
    }
    #endregion
}
