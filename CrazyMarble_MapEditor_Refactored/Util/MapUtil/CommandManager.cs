﻿using CrazyMarble_MapEditor.Properties;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CrazyMarble_MapEditor.Util
{
    public abstract class UndoableCommand
    {
        public abstract string Discription { get; }
        public abstract void Execute();
        public abstract void UnExecute();
    }

    public abstract class UndoablePropertyChangeCommandBase<T> : UndoableCommand {}

    public class UndoablePropertyChangeCommand<T> : UndoablePropertyChangeCommandBase<T>
    {
        #region Member
        private object _oldValue;
        private object _newValue;
        private string _property;
        private T _instance;
        #endregion

        /// <summary>
        /// Initialize a new instance of <see cref="UndoablePropertyChangeCommand"/>.
        /// </summary>
        /// <param name="property">The name of the property.</param>
        /// <param name="instance">The instance of the property.</param>
        /// <param name="oldValue">The pre-change property.</param>
        /// <param name="newValue">The post-change property.</param>
        public UndoablePropertyChangeCommand(string property, T instance, object oldValue, object newValue)
            : this(property, instance, oldValue, newValue, property)
        {

        }


        /// <summary>
        /// Initialize a new instance of <see cref="UndoablePropertyChangeCommand"/>.
        /// </summary>
        /// <param name="property">The name of the property.</param>
        /// <param name="instance">The instance of the property.</param>
        /// <param name="oldValue">The pre-change property.</param>
        /// <param name="newValue">The post-change property.</param>
        /// <param name="name">The name of the undo operation.</param>
        public UndoablePropertyChangeCommand(string property, T instance, object oldValue, object newValue, string name)
        {
            _instance = instance;
            _property = property;
            _oldValue = oldValue;
            _newValue = newValue;

            Name = name;
        }

        /// <summary>
        /// The property name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Undo the property change.
        /// </summary>
        public override void UnExecute()
        {
            _instance.GetType().GetProperty(_property).SetValue(_instance, _oldValue, null);
        }

        public override void Execute()
        {
            _instance.GetType().GetProperty(_property).SetValue(_instance, _newValue, null);
        }

        public override string Discription
        {
            get { return string.Format(Resources.CommandChangeNode, Name); }
        }
    }


    public class UndoablePropertiesChangeCommand<T> : UndoablePropertyChangeCommandBase<T>
    {
        #region Member
        private object[] _oldValue;
        private object[] _newValue;
        private string[] _property;
        private T _instance;
        #endregion

        /// <summary>
        /// Initialize a new instance of <see cref="UndoablePropertyChangeCommand"/>.
        /// </summary>
        /// <param name="property">The name of the property.</param>
        /// <param name="instance">The instance of the property.</param>
        /// <param name="oldValue">The pre-change property.</param>
        /// <param name="newValue">The post-change property.</param>
        public UndoablePropertiesChangeCommand(string[] property, T instance, object[] oldValue, object[] newValue)
            : this(property, instance, oldValue, newValue, property[0])
        {

        }


        /// <summary>
        /// Initialize a new instance of <see cref="UndoablePropertyChangeCommand"/>.
        /// </summary>
        /// <param name="property">The name of the property.</param>
        /// <param name="instance">The instance of the property.</param>
        /// <param name="oldValue">The pre-change property.</param>
        /// <param name="newValue">The post-change property.</param>
        /// <param name="name">The name of the undo operation.</param>
        public UndoablePropertiesChangeCommand(string[] property, T instance, object[] oldValue, object[] newValue, string name)
        {
            var list = new List<int>();
            //list.Add(property.Length);
            list.Add(oldValue.Length);
            list.Add(newValue.Length);
            var areArgumentCountEqual = true;
            foreach (int count in list)
            {
                if (count != property.Length)
                {
                    areArgumentCountEqual = false;
                    break;
                }
            }

            if (!areArgumentCountEqual)
            {
                throw new ArgumentException("Array arguments MUST have same length.");
            }

            _instance = instance;
            _property = property;
            _oldValue = oldValue;
            _newValue = newValue;

            Name = name;
        }

        /// <summary>
        /// The property name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Undo the property change.
        /// </summary>
        public override void UnExecute()
        {
            for (int i = 0; i < _property.Length; i++)
            {
                _instance.GetType().GetProperty(_property[i]).SetValue(_instance, _oldValue[i], null);
            }
        }

        public override void Execute()
        {
            for (int i = 0; i < _property.Length; i++)
            {
                _instance.GetType().GetProperty(_property[i]).SetValue(_instance, _newValue[i], null);
            }
        }

        public override string Discription
        {
            get { return string.Format(Resources.CommandChangeProperty, Name); }
        }
    }

    public class DropOutStack<T>
    {
        private T[] items;
        private int top = 0;
        public DropOutStack(int capacity)
        {
            items = new T[capacity];
        }

        public void Push(T item)
        {
            items[top] = item;
            top = (top + 1) % items.Length;
            Count = Math.Min(Count + 1, items.Length);
        }
        public T Pop()
        {
            if (IsEmpty()) throw new InvalidOperationException("The stack is empty.");

            top = (items.Length + top - 1) % items.Length;
            Count--;

            var elem = items[top];
            items[top] = default(T);
            return elem;
        }

        public T Peek(int indexFromTop = 0)
        {
            if (indexFromTop > Count) throw new InvalidOperationException("Invalid position from top.");
            if (IsEmpty()) throw new InvalidOperationException("The stack is empty.");

            return items[(items.Length + top - 1 - indexFromTop) % items.Length];
        }

        public int Count
        {
            get;
            private set;
        }

        public bool IsEmpty()
        {
            return Count == 0;
        }
        public void Clear()
        {
            top = 0;
            while (!IsEmpty())
            {
                Pop();
            }
        }
    }

    public class CommandManager
    {
        protected static DropOutStack<UndoableCommand> _Undocommands = new DropOutStack<UndoableCommand>(50);
        protected static DropOutStack<UndoableCommand> _Redocommands = new DropOutStack<UndoableCommand>(50);

        private static bool _isUndoRedoing;
        public static bool IsJournalEnabled = true;

        public static event EventHandler EnableDisableUndoRedoFeature;

        public static void Redo(int steps = 1)
        {
            if (IsRedoPossible(steps))
            {
                _isUndoRedoing = true;

                for (int i = 0; i < steps; i++)
                {
                    var command = _Redocommands.Pop();
                    _Undocommands.Push(command);

                    command.Execute();
                }

                _isUndoRedoing = false;
            }
            if (EnableDisableUndoRedoFeature != null)
            {
                EnableDisableUndoRedoFeature(null, null);
            }
        }

        public static void Undo(int steps = 1)
        {
            if (IsUndoPossible(steps))
            {
                _isUndoRedoing = true;

                for (int i = 0; i < steps; i++)
                {
                    var command = _Undocommands.Pop();
                    _Redocommands.Push(command);

                    command.UnExecute();
                }

                _isUndoRedoing = false;
            }
            if (EnableDisableUndoRedoFeature != null)
            {
                EnableDisableUndoRedoFeature(null, null);
            }
        }

        public static bool IsUndoPossible(int steps = 1)
        {
            return _Undocommands.Count >= steps;
        }

        public static bool IsRedoPossible(int steps = 1)
        {
            return _Redocommands.Count >= steps;
        }

        public static void ExecuteNewCommand(UndoableCommand cmd)
        {
            if (!_isUndoRedoing && IsJournalEnabled)
            {
                _Undocommands.Push(cmd);
                cmd.Execute();
                _Redocommands.Clear();
            }
            else
            {
                cmd.Execute();
            }
        }

        public static void ExecuteNewCommand<T>(UndoablePropertyChangeCommandBase<T> cmd)
        {
            if (!_isUndoRedoing && IsJournalEnabled)
            {
                _Undocommands.Push(cmd);
                _Redocommands.Clear();
            }
        }

        public static void ClearUndoables()
        {
            _Undocommands.Clear();
            _Redocommands.Clear();
        }

        public static string LastDoneCommand(int steps = 1)
        {
            return IsUndoPossible(steps) ? _Undocommands.Peek(steps - 1).Discription + " " : string.Empty;
        }

        public static string LastUndoneCommand(int steps = 1)
        {
            return IsRedoPossible(steps) ? _Redocommands.Peek(steps - 1).Discription + " " : string.Empty;
        }

        public class CommandListManager
        {
            public class CommandItem
            {
                private CommandItemCommand _commandItem;
                public String Description { get; private set; }
                public ICommand ExecuteCommand 
                {
                    get { return _commandItem; } 
                }

                public CommandItem(Action<object> onExecute, UndoableCommand command)
                {
                    _commandItem = new CommandItemCommand(onExecute);
                    Description = command.Discription;
                }
            }

            public class CommandItemCommand : ICommand
            {
                private Action<object> _execute;
                protected int _steps;
                public CommandItemCommand(Action<object> onExecute)
                {
                    _execute = onExecute;
                }

                public bool CanExecute(object parameter)
                {
                    return true;
                }

                public event EventHandler CanExecuteChanged;

                public void Execute(object parameter)
                {
                    _execute(parameter);
                }
            }

            public static ICollection<CommandItem> DoneCommands
            {
                get
                {
                    var commands = new List<CommandItem>();
                    for (int i = 0; i < CommandManager._Undocommands.Count; i++)
                    {
                        int steps = i + 1;
                        commands.Add(new CommandItem(x => CommandManager.Undo(steps), 
                            CommandManager._Undocommands.Peek(i)));
                    }
                    return commands;
                }
            }

            public static ICollection<CommandItem> UndoneCommands
            {
                get
                {
                    var commands = new List<CommandItem>();
                    for (int i = 0; i < CommandManager._Redocommands.Count; i++)
                    {
                        int steps = i + 1;
                        commands.Add(new CommandItem(x => CommandManager.Redo(steps), 
                            CommandManager._Redocommands.Peek(i)));
                    }
                    return commands;
                }
            }
        }
    }

    
}
