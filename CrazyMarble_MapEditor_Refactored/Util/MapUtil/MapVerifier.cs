﻿using CrazyMarble_MapEditor.Properties;
using CrazyMarble_MapEditor.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CrazyMarble_MapEditor.Util
{
    public class MapVerifier
    {
        public static bool verify(MapViewModel map)
        {
            var errorList = map.ErrorList;
            errorList.Clear();

            if (map.Nodes.Count == 0)
            {
                errorList.Add(Resources.CompileErrorNoNode);
            }

            foreach (NodeViewModel node in map.Nodes)
            {
                if (node.X < 0 || node.X > map.BackgroundSize.Width - node.Width ||
                    node.Y < 0 || node.Y > map.BackgroundSize.Height - node.Height)
                {
                    map.ErrorList.Add(string.Format(
                        Resources.CompileErrorNodeOutOfBackground, node.ID, new Point(node.X, node.Y)));
                }
            }

            foreach (EdgeViewModel edge in map.Edges)
            {
                if (edge.End == null)
                {
                    var start = edge.Start as NodeViewModel;
                    string edgeTargetType = string.Empty;
                    switch (edge.Type)
                    {
                        case EdgeType.Next:
                            edgeTargetType = Resources.EdgeTypeNext;
                            break;
                        case EdgeType.NextIfArrived:
                            edgeTargetType = Resources.EdgeTypeNextIfArrived;
                            break;
                        case EdgeType.JumpTo:
                            edgeTargetType = Resources.EdgeTypeJumpTo;
                            break;
                        default:
                            break;
                    }
                    map.ErrorList.Add(string.Format(
                        Resources.ComplieErrorNextNotSet, start.ID, edgeTargetType));
                }
            }

            if (map.Background == null)
            {
                errorList.Add(Resources.CompileErrorNoBackground);
            }
            else if (map.BackgroundPath != null && !File.Exists(map.BackgroundPath))
            {
                errorList.Add(string.Format(Resources.ComplieErrorBackgroundFileNotExist,
                    map.BackgroundPath));
            }

            if (errorList.Count == 0)
            {
                errorList.Add(Resources.CompileErrorNoError);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
