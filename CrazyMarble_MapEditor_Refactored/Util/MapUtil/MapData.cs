﻿using CrazyMarble_MapEditor.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrazyMarble_MapEditor.Util
{
    public class MapData
    {
        #region Constructor
        public MapData(MapViewModel map = null)
        {
            if (map != null)
            {
                MapName = map.MapName;
                Nodes = map.Nodes;
            }
        }
        #endregion

        #region Data Members
        public string MapName;
        public BindingList<NodeViewModel> Nodes;
        #endregion
    }
}
