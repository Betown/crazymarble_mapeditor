﻿using CrazyMarble_MapEditor.Properties;
using CrazyMarble_MapEditor.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace CrazyMarble_MapEditor.Util
{
    public class FileManager
    {
        private const int MAX_BACKGROUNDSIZE = 4096;
        private const int MIN_BACKGROUNDSIZE = 400;

        public FileManager(MapViewModel map, IUIService UIservice, IRecentFileService recentFileService)
        {
            _map = map;
            _UIservice = UIservice;
            _recentFileService = recentFileService;
        }

        #region Members

        private MapViewModel _map;
        private IUIService _UIservice;
        private IRecentFileService _recentFileService;

        private bool _useEmptyBackground;
        private bool _isDirty;
        private bool _isBackgroundDirty;

        #endregion

        #region Properties

        public bool IsDirty
        {
            get { return _isDirty; }
            set { _isDirty = value; }
        }

        public bool UseEmptyBackground
        {
            get { return _useEmptyBackground; }
            set { _useEmptyBackground = value; }
        }

        public Size MaxBackgroundSize
        {
            get { return new Size(MAX_BACKGROUNDSIZE, MAX_BACKGROUNDSIZE); }
        }
        public Size MinBackgroundSize
        {
            get { return new Size(MIN_BACKGROUNDSIZE, MIN_BACKGROUNDSIZE); }
        }

        #endregion

        #region Helper

        public void cleanUp()
        {
            _map.clearMapState();
            _map.LastFilePath = null;
            IsDirty = false;
        }

        #endregion

        #region Map Data File Operation

        #region Read Map Data

        public bool readFile(string filename)
        {
            if (!_previewReadFile(filename))
            {
                return false;
            }

            cleanUp();

            if (!loadBackground(filename + ".png"))
            {
                cleanUp();
                return false;
            }

            _readFile(filename);

            _reviewReadFile(); 

            _isBackgroundDirty = false;
            _map.LastFilePath = filename;
            _map.IsDirty = false;
            return true;
        }

        // Does background file success?
        private bool _previewReadFile(string filename)
        {
            if (!File.Exists(filename + ".png"))
            {
                _UIservice.MsgBox(
                    string.Format(Resources.MsgNoBackgroundFileText, filename),
                    Resources.MsgNoBackgroundFileTitle,
                    MessageBoxImage.Error,
                    MessageBoxButton.OK);
                return false;
            }

            return true;
        }

        // Precondition: filename must exist and its content is valid
        // Postcondition: file loaded on MapViewModel
        private bool _readFile(string filename)
        {
            try
            {
                using (_UIservice.BusyCursor())
                {
                    using (StreamReader file = new StreamReader(filename))
                    {
                        using (JsonReader reader = new JsonTextReader(file))
                        {
                            _map.Status = string.Format(Resources.StatusLoading, filename);

                            var serializer = new JsonSerializer();
                            var map = serializer.Deserialize<MapData>(reader);

                            // Reconstruct map data based on node values
                            _map.MapName = map.MapName;
                            foreach (NodeViewModel node in map.Nodes)
                            {
                                _map.Nodes.Add(node);
                            }
                            foreach (NodeViewModel node in _map.Nodes)
                            {
                                var edge = new EdgeViewModel(node, EdgeType.Next);
                                edge.Connect(_map.Nodes[node.Next.Value]);
                                _map.Edges.Add(edge);

                                if (node.NextIfArrived.HasValue)
                                {
                                    edge = new EdgeViewModel(node, EdgeType.NextIfArrived);
                                    edge.Connect(_map.Nodes[node.NextIfArrived.Value]);
                                    _map.Edges.Add(edge);
                                }

                                if (node.JumpTo.HasValue)
                                {
                                    edge = new EdgeViewModel(node, EdgeType.JumpTo);
                                    edge.Connect(_map.Nodes[node.JumpTo.Value]);
                                    _map.Edges.Add(edge);
                                }
                            }

                            // Clear all connection history
                            CrazyMarble_MapEditor.Util.CommandManager.ClearUndoables();

                            _map.Status = Resources.StatusReady;
                        }
                    }
                }
            }
            catch (Exception)
            {
                _UIservice.MsgBox(
                    string.Format(Resources.MsgFileReadFailureText, filename),
                    Resources.MsgFileReadFailureTitle,
                    MessageBoxImage.Error,
                    MessageBoxButton.OK
                    );
                cleanUp();
                return false;
            }

            _recentFileService.AddHistory(filename);
            return true;
        }

        // Are loaded map data valid?
        private void _reviewReadFile()
        {
            checkMapValidity(); 
        }

        #endregion

        #region Write Map Data

        public bool writeFile(string filename)
        {
            try
            {
                using (_UIservice.BusyCursor())
                {
                    _writeFile(filename);
                }
            }
            catch (Exception)
            {
                return false;
            }

            if (!_reviewWriteFile(filename))
            {
                return false;
            }

            _recentFileService.AddHistory(filename);
            _map.IsDirty = false;
            return true;
        }

        private void _writeFile(string filename)
        {
            using (StreamWriter file = new StreamWriter(filename))
            {
                using (JsonWriter writer = new JsonTextWriter(file))
                {
                    _map.Status = string.Format(Resources.StatusSaving, filename);

                    var setting = new JsonSerializerSettings();
                    setting.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

                    var serializer = JsonSerializer.Create(setting);
                    serializer.Formatting = Formatting.Indented;

                    var data = new MapData(_map);
                    serializer.Serialize(writer, data);
                    _map.Status = Resources.StatusReady;
                }
            }
        }

        private bool _reviewWriteFile(string filename)
        {
            if (_isBackgroundDirty)
            {
                return saveBackground(filename);
            }

            return true;
        }

        #endregion

        #endregion

        #region Background File Operation

        #region Load Background

        private bool loadBackground(string filename)
        {
            var image = _loadBackground(filename);
            if (_reviewLoadBackground(image, filename))
            {
                _map.BackgroundPath = filename;
                _map.Background = image;
                return true;
            }

            return false;
        }

        private bool loadEmptyBackground(Size size)
        {
            var image = _loadEmptyBackground(size);
            if (_reviewLoadBackground(image, Resources.PropEmptyBackgroundName))
            {
                _map.BackgroundPath = null;
                _map.Background = image;
                return true;
            }
            return false;
        }

        private bool _reviewLoadBackground(BitmapSource image, string filename)
        {
            if (image.PixelWidth < MIN_BACKGROUNDSIZE || image.PixelHeight < MIN_BACKGROUNDSIZE ||
                image.PixelWidth > MAX_BACKGROUNDSIZE || image.PixelHeight > MAX_BACKGROUNDSIZE)
            {
                _UIservice.MsgBox(
                    string.Format(Resources.MsgBackgroundInvaidSizeText, 
                    filename, MIN_BACKGROUNDSIZE, MAX_BACKGROUNDSIZE),
                    Resources.MsgBackgroundInvaidSizeTitle,
                    MessageBoxImage.Error,
                    MessageBoxButton.OK);
                return false;
            }
            if (image.PixelWidth < _map.BackgroundSize.Width || image.PixelHeight < _map.BackgroundSize.Height)
            {
                var proceedLoadEvenClipping =
                       _UIservice.MsgBox(Resources.MsgWarningBackgroundClippedText,
                       Resources.MsgWarningBackgroundClippedTitle,
                       MessageBoxImage.Warning,
                       MessageBoxButton.YesNo
                       );

                if (proceedLoadEvenClipping == MessageBoxResult.No)
                {
                    return false;
                }
            }
            return true;
        }

        private BitmapSource _loadBackground(string filename)
        {
            var image = new BitmapImage();
            using (FileStream stream = File.OpenRead(filename))
            {
                image.BeginInit();
                image.StreamSource = stream;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.EndInit(); // load the image from the stream
            } // close the stream

            return image;
        }

        private BitmapSource _loadEmptyBackground(Size size)
        {
            int width = (int)size.Width;
            int height = (int)size.Height;

            int stride = width / 8 + 7;
            byte[] pixels = new byte[height * stride];

            var palette = new BitmapPalette(new List<System.Windows.Media.Color>() { System.Windows.Media.Colors.White });

            var bitmap = BitmapSource.Create(width, height, 72.0, 72.0,
                System.Windows.Media.PixelFormats.Indexed1, palette, pixels, stride);

            return bitmap;
        }

        #endregion

        #region Save Background

        public bool saveBackground(string filename)
        {
            try
            {
                using (FileStream stream = new FileStream(filename + ".png", FileMode.Create))
                {
                    var encoder = new PngBitmapEncoder();

                    encoder.Frames.Add(BitmapFrame.Create(_map.Background));
                    encoder.Save(stream);
                }

                _isBackgroundDirty = false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #endregion

        #region Check Validity

        private bool checkMapValidity()
        {
            bool verifySuccess = _map.verify();

            if (!verifySuccess)
            {
                _UIservice.ShowErrorWindow();
            }

            return verifySuccess;
        }

        #endregion

        #region Command Implementations

        #region Save Confirmation

        public bool confirmSave()
        {
            if (IsDirty)
            {
                var result = _UIservice.MsgBox(
                    Resources.MsgConfirmSaveText,
                    Resources.MsgConfirmSaveTitle,
                    MessageBoxImage.Question,
                    MessageBoxButton.YesNoCancel);

                if (result == MessageBoxResult.Cancel)
                {
                    return false;
                }
                else if (result == MessageBoxResult.Yes)
                {
                    return save();
                }
                else if (result == MessageBoxResult.No)
                {
                    return true;
                }
                else { return false; }
            }
            else return true;
        }

        #endregion

        #region New File

        public void newFile()
        {
            if (confirmSave())
            {
                cleanUp();
                IsDirty = false;
            }
        }

        #endregion

        #region Open File

        public void openFile()
        {
            if (!confirmSave()) return;

            // Configure open file dialog box 
            var filename =
                _UIservice.FileDialog(
                string.Empty,
                ".json",
                Resources.FileDialogMapTypeDescription,
                IOMode.Read);

            // Process open file dialog box results 
            if (filename != null)
            {
                readFile(filename);
            }
        }
        public void openFileFromRecent(string filepath)
        {
            if (!confirmSave()) return;

            if (File.Exists(filepath) && readFile(filepath))
            {
            }
            else
            {
                var deleteOrNot =
                    _UIservice.MsgBox(
                    string.Format(Resources.MsgFileRecentFailureText, filepath),
                    Resources.MsgFileRecentFailureTitle,
                    MessageBoxImage.Error,
                    MessageBoxButton.YesNo
                    );

                if (deleteOrNot == MessageBoxResult.Yes)
                {
                    _recentFileService.RemoveHistory(filepath);
                }
            }
        }

        #endregion

        #region Save File

        public bool save()
        {
            if (checkMapValidity())
            {
                if (_map.LastFilePath == null)
                {
                    return saveAs();
                }
                else
                {
                    return writeFile(_map.LastFilePath);
                }
            }
            return false;
        }
        public bool saveAs()
        {
            if (checkMapValidity())
            {
                // Show save file dialog box
                // Process save file dialog box results
                var filename =
                    _UIservice.FileDialog(
                    _map.MapName,
                    ".json",
                    Resources.FileDialogMapTypeDescription,
                    IOMode.Write);

                if (filename != null)
                {
                    return writeFile(filename);
                }
            }
            return false;
        }

        #endregion

        #region Set Background

        public void setBackGroundFromImage()
        {
            var filename =
                _UIservice.FileDialog(
                _map.LastFilePath == null ? string.Empty : _map.LastFilePath + ".png",
                ".png",
                Resources.FileDialogBackgroundTypeDescription,
                IOMode.Read);

            // Process open file dialog box results 
            if (filename != null)
            {
                loadBackground(filename);
                _isBackgroundDirty = true;
                _isDirty = true;
            }
        }

        public void setEmptyBackGround(Size size)
        {
            loadEmptyBackground(size);
            _isBackgroundDirty = true;
            _isDirty = true;
        }

        #endregion

        #endregion
    }
}
