﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CrazyMarble_MapEditor.Util
{
    [JsonConverter(typeof(IntegerJsonConverter))]
    public class Integer
    {
        public int Value { get; set; }
        public Integer(int value)
        {
            this.Value = value;
        }
        public static implicit operator Integer(int value)
        {
            return new Integer(value);
        }
        public static implicit operator int(Integer integer)
        {
            return integer.Value;
        }
        public static implicit operator int?(Integer integer)
        {
            if (integer == null) return null;
            else return integer.Value; 
        }
        public static implicit operator Integer(double value)
        {
            return new Integer((int)Math.Round(value));
        }
        public static implicit operator double(Integer integer)
        {
            return integer.Value;
        }

        public static Integer operator ++(Integer integer)
        {
            integer.Value++;
            return integer;
        }
        public static Integer operator --(Integer integer)
        {
            integer.Value--;
            return integer;
        }
        public static Integer operator +(Integer integer, int value)
        {
            integer.Value += value;
            return integer;
        }
        public static Integer operator -(Integer integer, int value)
        {
            integer.Value -= value;
            return integer;
        }

        public override bool Equals(object obj)
        {
            if (obj is Integer)
            {
                return Value == (obj as Integer).Value;
            }
            else return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }

    [ValueConversion(typeof(Integer), typeof(Double))]
    public class IntegerToDoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var i = value as Integer;
            return (double)i;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class IntegerJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JToken t = JToken.FromObject((value as Integer).Value);
            t.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = serializer.Deserialize<int?>(reader);
            if (value.HasValue) return new Integer(value.Value);
            else return null;
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Integer);
        }
    }
}
