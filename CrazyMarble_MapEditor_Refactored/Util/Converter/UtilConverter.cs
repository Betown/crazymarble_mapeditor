﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace CrazyMarble_MapEditor.Util
{
    [ValueConversion(typeof(Size),typeof(Rect))]
    public class SizeToRectConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new Rect((Size)value);
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var rect = (Rect)value;
            return new Size(rect.Width, rect.Height);
        }
    }

    [ValueConversion(typeof(Point), typeof(Rect))]
    public class PointToRectConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new Rect((Point)value, new Size());
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var rect = (Rect)value;
            return new Point(rect.Left, rect.Top);
        }
    }

    public class NodePropertiesWindowPlacementConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double multiplier = 1.0;
            Vector offset = new Vector();
            if (values.Length >= 9)
            {
                multiplier = (double)values[8];
            }
            if (values.Length >= 3)
            {
                var horizontalAlignment = (HorizontalAlignment)values[2];
                var verticalAlignment = (VerticalAlignment)values[3];

                if (horizontalAlignment == HorizontalAlignment.Center ||
                    horizontalAlignment == HorizontalAlignment.Stretch)
                {
                    offset.X = Math.Max(0, (double)values[4] - (double)values[6]);
                }
                if (verticalAlignment == VerticalAlignment.Center ||
                    verticalAlignment == VerticalAlignment.Stretch)
                {
                    offset.Y = Math.Max(0, (double)values[5] - (double)values[7]);
                }
                offset /= 2.0;
            }

            if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue)
            {
                return new Rect();
            }

            double x = (double)(int)values[0] * multiplier + offset.X;
            double y = (double)(int)values[1] * multiplier + offset.Y;
            return new Rect(new Point(x, y), new Size());
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
