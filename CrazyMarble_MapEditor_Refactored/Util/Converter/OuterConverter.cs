﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CrazyMarble_MapEditor.Util
{
    public abstract class InnerConverter : IValueConverter
    {
        public InnerConverter()
        {}

        public abstract object Convert(
            object value, Type targetType, object parameter, System.Globalization.CultureInfo culture);

        public abstract object ConvertBack(
            object value, Type targetType, object parameter, System.Globalization.CultureInfo culture);
    }

    public class OuterConverter<T> : IValueConverter
        where T : InnerConverter
    {
        private T _converter = default(T);
        private void CreateConverter()
        {
            if (_converter == null)
            {
                _converter = (T)Activator.CreateInstance(typeof(T));
            }
        }

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            CreateConverter();
            return _converter.Convert(value, targetType, parameter, culture);
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            CreateConverter();
            return _converter.ConvertBack(value, targetType, parameter, culture);
        }
    }

    public abstract class InnerMultiConverter : IMultiValueConverter
    {
        public InnerMultiConverter() { }

        public abstract object Convert(
            object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture);

        public abstract object[] ConvertBack(
            object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture);
    }

    public class OuterMultiConverter<T> : IMultiValueConverter
       where T : InnerMultiConverter
    {
        private T _converter = default(T);
        private void CreateConverter()
        {
            if (_converter == null)
            {
                _converter = (T)Activator.CreateInstance(typeof(T));
            }
        }

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            CreateConverter();
            return _converter.Convert(values, targetType, parameter, culture);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            CreateConverter();
            return _converter.ConvertBack(value, targetTypes, parameter, culture);
        }
    }
}
