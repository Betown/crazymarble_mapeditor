﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace CrazyMarble_MapEditor.Util
{

    public class SizeToGridPointConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Size size = (Size)values[0];
            switch ((string)values[1])
            {
                case "HorizontalStart":
                    return new Point(0, 0);
                case "HorizontalEnd":
                    return new Point(size.Width, 0);
                case "VerticalStart":
                    return new Point(0, 0);
                case "VerticalEnd":
                    return new Point(0, size.Height);

                case "LeftStart":
                    return new Point(1, 1);
                case "LeftEnd":
                    return new Point(1, size.Height - 1);

                case "TopStart":
                    return new Point(1, 1);
                case "TopEnd":
                    return new Point(size.Width - 1, 1);

                case "RightStart":
                    return new Point(size.Width - 1, 1);
                case "RightEnd":
                    return new Point(size.Width - 1, size.Height - 1);

                case "BottomStart":
                    return new Point(1, size.Height - 1);
                case "BottomEnd":
                    return new Point(size.Width - 1, size.Height - 1);

                default:
                    throw new NotImplementedException();
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
